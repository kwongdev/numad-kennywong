/***
 * Excerpted from "Hello, Android",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material, 
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose. 
 * Visit http://www.pragmaticprogrammer.com/titles/eband3 for more book information.
***/
package edu.neu.madcourse.kennywong.bananagrams;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import edu.neu.madcourse.kennywong.R;
import edu.neu.madcourse.kennywong.utils.Wordlist;

public class BananagramsMenu extends Activity implements OnClickListener {
   private static final String TAG = "Bananagrams";
   
   /** Called when the activity is first created. */
   @Override
   public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.bananagrams_menu);
      
      // Set up click listeners for all the buttons
      View newButton = findViewById(R.id.bananagrams_new_button);
      newButton.setOnClickListener(this);
      
      SharedPreferences settings = getSharedPreferences(SinglePlayerGame.BANANA_PREFS, 0);
      
      View continueButton = findViewById(R.id.bananagrams_continue_button);
      boolean continueGame = settings.getBoolean(SinglePlayerGame.CONTINUE_GAME, false);
      if (continueGame) {
    	  continueButton.setVisibility(View.VISIBLE);
    	  continueButton.setOnClickListener(this);
      } else {
    	  continueButton.setVisibility(View.GONE);
      }
      
      
      View helpButton = findViewById(R.id.bananagrams_help_button);
      helpButton.setOnClickListener(this);
      View acknowledgementButton = findViewById(R.id.bananagrams_acknowledgement_button);
      acknowledgementButton.setOnClickListener(this);
      View exitButton = findViewById(R.id.bananagrams_exit_button);
      exitButton.setOnClickListener(this);
      
      Wordlist.tryLoadResources(this);
   }

   @Override
   protected void onResume() {
      super.onResume();
      Music.play(this, R.raw.bananagrams_game, true);
      
      SharedPreferences settings = getSharedPreferences(SinglePlayerGame.BANANA_PREFS, 0);
      
      View continueButton = findViewById(R.id.bananagrams_continue_button);
      boolean continueGame = settings.getBoolean(SinglePlayerGame.CONTINUE_GAME, false);
      if (continueGame) {
    	  continueButton.setVisibility(View.VISIBLE);
    	  continueButton.setOnClickListener(this);
      } else {
    	  continueButton.setVisibility(View.GONE);
      }
   }

   @Override
   protected void onPause() {
      super.onPause();
      Music.stop(this);
   }

   public void onClick(View v) {
	   switch (v.getId()) {
	   case R.id.bananagrams_new_button:
		   openNewGameDialog();
		   break;
	   case R.id.bananagrams_continue_button:
		   continueGame();
		   break;
	   case R.id.bananagrams_help_button:
		   Intent helpIntent = new Intent(this, Help.class);
		   startActivity(helpIntent);
		   break;
	   case R.id.bananagrams_acknowledgement_button:
		   Intent acknowledgementsIntent = new Intent(this, Acknowledgements.class);
		   startActivity(acknowledgementsIntent);
		   break;
	   case R.id.bananagrams_exit_button:
		   finish();
		   break;
	   }
   }
   
   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      super.onCreateOptionsMenu(menu);
      MenuInflater inflater = getMenuInflater();
      inflater.inflate(R.menu.bananagrams_menu, menu);
      return true;
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      switch (item.getItemId()) {
      case R.id.bananagrams_settings:
         startActivity(new Intent(this, Prefs.class));
         return true;
      }
      return false;
   }

   /** Ask the user what difficulty level they want */
   private void openNewGameDialog() {
      new AlertDialog.Builder(this)
           .setTitle(R.string.bananagrams_new_game_title)
           .setItems(R.array.bananagrams_difficulty,
            new DialogInterface.OnClickListener() {
               public void onClick(DialogInterface dialoginterface,
                     int i) {
            	   startGame(i);
               }
            })
           .show();
   }

   /** Start a new game with the given difficulty level **/
   private void startGame(int i) {
      Log.d(TAG, "Starting new game with difficulty " + i);
      Intent intent = new Intent(this, SinglePlayerGame.class);
      intent.putExtra(SinglePlayerGame.KEY_DIFFICULTY, i);
      startActivity(intent);
   }

   /** Continue a saved game **/
   private void continueGame() {
	   Log.d(TAG, "Continue saved game");
	   Intent intent = new Intent(this, SinglePlayerGame.class);
	   intent.putExtra(SinglePlayerGame.CONTINUE_GAME, true);
	   startActivity(intent);
   }
}