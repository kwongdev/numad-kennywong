package edu.neu.madcourse.kennywong.bananagrams.data;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Random;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class BananagramsLetters {
	public static LinkedHashMap<Character, Integer> bunch = new LinkedHashMap<Character, Integer>();
	public static HashMap<Character, Integer> playerLetters = new HashMap<Character, Integer>();

	public static void initialize() {
		bunch = new LinkedHashMap<Character, Integer>();
		bunch.put('A', 13);
		bunch.put('B', 3);
		bunch.put('C', 3);
		bunch.put('D', 6);
		bunch.put('E', 18);
		bunch.put('F', 3);
		bunch.put('G', 4);
		bunch.put('H', 3);
		bunch.put('I', 12);
		bunch.put('J', 2);
		bunch.put('K', 2);
		bunch.put('L', 5);
		bunch.put('M', 3);
		bunch.put('N', 8);
		bunch.put('O', 11);
		bunch.put('P', 3);
		bunch.put('Q', 2);
		bunch.put('R', 9);
		bunch.put('S', 6);
		bunch.put('T', 9);
		bunch.put('U', 6);
		bunch.put('V', 3);
		bunch.put('W', 3);
		bunch.put('X', 2);
		bunch.put('Y', 3);
		bunch.put('Z', 2);
		
		playerLetters = new HashMap<Character, Integer>();
		
		drawLetter(21);
	}
	
	public static String storeBunch() {
		Gson gson = new Gson();
		return gson.toJson(bunch);
	}
	
	public static String storePlayerLetters() {
		Gson gson = new Gson();
		return gson.toJson(playerLetters);
	}
	
	public static void loadBunch(String serializedBunch) {
		Gson gson = new Gson();
		Type hashType = new TypeToken<LinkedHashMap<Character,Integer>>() {}.getType();
		
		if (serializedBunch != null) {
			bunch = gson.fromJson(serializedBunch, hashType);
		}
	}
	
	public static void loadPlayerLetters(String serializedPlayerLetters) {
		Gson gson = new Gson();
		Type hashType = new TypeToken<HashMap<Character,Integer>>() {}.getType();
		
		if (serializedPlayerLetters != null) {
			playerLetters = gson.fromJson(serializedPlayerLetters, hashType);	
		}
	}
	
	public static ArrayList<Character> bunchList() {
		ArrayList<Character> res = new ArrayList<Character>();
		for (int i = 0; i < bunch.size(); i++) {
			   Character letter = (new ArrayList<Character>(bunch.keySet())).get(i);

			   for (int j = 0; j < bunch.get(letter); j++) {
				   res.add(letter);
			   }
		   }
		
		return res;
	}
	
	public static ArrayList<Character> sortedPlayerLettersList() {
		ArrayList<Character> res = playerLettersList();
		
		Collections.sort(res);
		
		return res;
	}
	
	public static ArrayList<Character> playerLettersList() {
		ArrayList<Character> res = new ArrayList<Character>();
		for (int i = 0; i < playerLetters.size(); i++) {
			Character letter = (new ArrayList<Character>(playerLetters.keySet())).get(i);

			for (int j = 0; j < playerLetters.get(letter); j++) {
				res.add(letter);
			}
		}

		return res;
	}
	
	public static void returnLetter(Character c) {
		if (c != ' ') {
			if (playerLetters.get(c) != null) {
				playerLetters.put(c, playerLetters.get(c) + 1);
			} else {
				playerLetters.put(c, 1);
			}
		}
	}
	
	public static void useLetter(Character c) {
		playerLetters.put(c, playerLetters.get(c) - 1);
	}
	
	public static void useLetterSinglePlayer(Character c) {
		useLetter(c);
		drawLetter(1);
	}
	
	public static void dump(Character c) {
		playerLetters.put(c, playerLetters.get(c) - 1);
		bunch.put(c, bunch.get(c) + 1);
		
		drawLetter(3);
	}

	public static void peel() {
		drawLetter(1);
	}
	
	public static boolean isGameOver() {
		boolean res = true;
		
		for (int i = 0; i < bunch.size(); i++) {
			Character letter = (new ArrayList<Character>(bunch.keySet())).get(i);

			res &= (bunch.get(letter) == 0);
		}

		return res;
	}

	private static void drawLetter(int numLetters) {
		Random randomGen = new Random();
		for (int i = 0; i < numLetters; i++) {
			int idx = randomGen.nextInt(bunchList().size());
			Character letter = bunchList().get(idx);

			if (bunch.get(letter) > 0) {
				if (!playerLetters.containsKey(letter)) {
					playerLetters.put(letter, 1);
				} else {
					playerLetters.put(letter, playerLetters.get(letter) + 1);
				}

				bunch.put(letter, bunch.get(letter) - 1);
			} else {
				numLetters--;
			}
		}
	}
}
