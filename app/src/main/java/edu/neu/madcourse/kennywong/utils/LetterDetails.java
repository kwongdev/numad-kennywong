package edu.neu.madcourse.kennywong.utils;

public class LetterDetails {
	private char letter;
	private int wordLength;
	
	public LetterDetails(char letter, int wordLength) {
		this.setLetter(letter);
		this.setWordLength(wordLength);
	}

	public char getLetter() {
		return letter;
	}

	public void setLetter(char letter) {
		this.letter = letter;
	}

	public int getWordLength() {
		return wordLength;
	}

	public void setWordLength(int wordLength) {
		this.wordLength = wordLength;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof LetterDetails))
            return false;
        if (obj == this)
            return true;

        LetterDetails rhs = (LetterDetails) obj;
        return (getLetter() == rhs.getLetter()) && (getWordLength() == rhs.getWordLength());
	}
	
	@Override
	public int hashCode() {
		return Character.getNumericValue(getLetter()) * getWordLength();
    }
}
