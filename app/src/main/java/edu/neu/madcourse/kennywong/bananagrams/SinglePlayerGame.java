/***
 * Excerpted from "Hello, Android",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material, 
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose. 
 * Visit http://www.pragmaticprogrammer.com/titles/eband3 for more book information.
***/
package edu.neu.madcourse.kennywong.bananagrams;

import java.util.ArrayList;
import java.util.Locale;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import edu.neu.madcourse.kennywong.R;
import edu.neu.madcourse.kennywong.bananagrams.data.BananagramsLetters;
import edu.neu.madcourse.kennywong.bananagrams.data.BananagramsTile;
import edu.neu.madcourse.kennywong.communication.CommunicationMenu;
import edu.neu.madcourse.kennywong.utils.Utils;

public class SinglePlayerGame extends Activity implements OnClickListener {
   private static final String TAG = "BananagramsGame";

   public static final String KEY_DIFFICULTY = "bananagrams_difficulty";

   public static final String BANANA_PREFS = "BananagramsPrefs";
   public static final String CONTINUE_GAME = "continue_game";
   public static final String LAST_BOARD = "last_board";
   public static final String TIME_LEFT = "time_remaining";
   public static final String POINTS = "points";
   public static final String BUNCH = "bunch";
   public static final String PLAYER_LETTERS = "player_letters";

   public static final int DIFFICULTY_EASY = 0;
   public static final int DIFFICULTY_MEDIUM = 1;
   public static final int DIFFICULTY_HARD = 2;
   
   public static String playerTiles;
   public static String bunchTiles;
   public static String boardTiles;

   private static final long MAX_TIME = 10 * 60 * 1000; // 10 minutes
   private static final int WARNING_TIME = 30; // in seconds
   private CountDownTimer timer;
   private long timeRemaining;
   
   public static int pointsScored = 0;
   
   private int boardPosition = -1;
   private ArrayList<View> playerTileButtons = new ArrayList<View>();
   
   private boolean undo = false;
   
   private static GridAdapter gridAdapter;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.bananagrams_game);
      Log.d(TAG, "onCreate");

      int diff = getIntent().getIntExtra(KEY_DIFFICULTY, -1);
      gridAdapter = new GridAdapter(this);
      
      BananagramsLetters.initialize();
      GridAdapter.getBlankTiles();
      
      // Continue-specific values
      if (getIntent().getBooleanExtra(CONTINUE_GAME, false)) {
    	  Log.v(TAG, "Continuing game");
    	  SharedPreferences settings = getSharedPreferences(BANANA_PREFS, 0);
    	  timeRemaining = settings.getLong(TIME_LEFT, MAX_TIME);
    	  
    	  String tilesAsString = BananagramsTile.toString(GridAdapter.tiles);
    	  GridAdapter.tiles = BananagramsTile.fromString(settings.getString(LAST_BOARD, tilesAsString));
    	  BananagramsLetters.loadBunch(settings.getString(BUNCH, BananagramsLetters.storeBunch()));
    	  BananagramsLetters.loadPlayerLetters(settings.getString(PLAYER_LETTERS, BananagramsLetters.storePlayerLetters()));
      } else {
    	  determineTimeDifficulty(diff);
      }


      TextView score = (TextView)findViewById(R.id.bananagrams_score);
      pointsScored = 0;
      score.setText(String.valueOf(pointsScored));
      
      GridView grid = (GridView)findViewById(R.id.bananagrams_board);
      grid.setAdapter(gridAdapter);
      grid.setOnItemClickListener(new OnItemClickListener() {
          public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
        	  if (undo) {
        		  BananagramsLetters.returnLetter(GridAdapter.tiles.get(position).getChar());
        		  GridAdapter.tiles.set(position, new BananagramsTile());
        		  
        		  updatePlayerTileButtons();
        	  } else {
	        	  if (boardPosition == -1) {
	        		  GridAdapter.tiles.get(position).setSelected(true);
	        		  boardPosition = position;
	        	  } else if (boardPosition == position) {
	        		  GridAdapter.tiles.get(boardPosition).setSelected(false);
	        		  boardPosition = -1;
	        	  } else {
	        		  GridAdapter.tiles.get(boardPosition).setSelected(false);
	        		  GridAdapter.tiles.get(position).setSelected(true);
	        		  boardPosition = position;
	        	  }
        	  }
        	  
    		  updateGrid();
          }
      });
      
      SharedPreferences settings = getSharedPreferences(BANANA_PREFS, 0);
      settings.edit().putBoolean(CONTINUE_GAME, true).apply();
      
      View pauseButton = findViewById(R.id.bananagrams_pause_button);
      pauseButton.setOnClickListener(this);
      View saveButton = findViewById(R.id.bananagrams_save_game_button);
      saveButton.setOnClickListener(this);
      View finishButton = findViewById(R.id.bananagrams_finish_button);
      finishButton.setOnClickListener(this);
      
      updatePlayerTileButtons();
   }

   @Override
   protected void onResume() {
      super.onResume();
      
      final TextView timerText = (TextView)findViewById(R.id.bananagrams_timer);
      timer = new CountDownTimer(timeRemaining, 1000) {
    	  public void onTick(long millisUntilFinished) {
    		  long secondsUntilFinished = millisUntilFinished / 1000;
    		  long minutes = secondsUntilFinished / 60;
    		  long seconds = secondsUntilFinished % 60;
    		  timerText.setText(minutes + ":" + String.format("%02d", seconds));
    		  
    		  TextView score = (TextView)findViewById(R.id.bananagrams_score);
    	      score.setText(String.valueOf(pointsScored));
    		  
    		  if (secondsUntilFinished < WARNING_TIME) {
    			  timerText.setTextColor(Color.RED);
    		  }
    		  
    		  timeRemaining = millisUntilFinished;
    	  }

    	  public void onFinish() {
    		  timerText.setText("Time's up!");
    		  gameOver();
    	  }
      }.start();
   }

   @Override
   protected void onPause() {
	   Log.d(TAG, "onPause");
	   SharedPreferences settings = getSharedPreferences(BANANA_PREFS, 0);
	   Editor editor = settings.edit();
	   editor.putLong(TIME_LEFT, timeRemaining).apply();
	   editor.putString(LAST_BOARD, BananagramsTile.toString(GridAdapter.tiles)).apply();
	   editor.putString(BUNCH, BananagramsLetters.storeBunch()).apply();
	   editor.putString(PLAYER_LETTERS, BananagramsLetters.storePlayerLetters()).apply();
	   // Stop timer
	   timer.cancel();
	   super.onPause();
   }
   
   protected void updateGrid() {
	   if (gridAdapter != null) {
		   Log.v(TAG, "Updating grid");
		   gridAdapter.notifyDataSetChanged();
		   
		   playerTiles = BananagramsLetters.storePlayerLetters();
		   bunchTiles = BananagramsLetters.storeBunch();
		   boardTiles = BananagramsTile.toString(GridAdapter.tiles);

		   if (CommunicationMenu.instantiated)
			   CommunicationMenu.storeGame();
	   } else {
		   Log.v(TAG, "Could not update grid");
	   }
   }
   
   private void updatePlayerTileButtons() {
	   createPlayerTileViews();
	   setPlayerTileListeners();
   }
   
   private void createPlayerTileViews() {
	   playerTileButtons.clear();
	   LinearLayout ll = (LinearLayout)findViewById(R.id.bananagrams_keypad);
	   ll.removeAllViews();
	   
	   for (Character c : BananagramsLetters.sortedPlayerLettersList()) {
		   Button b = new Button(this);
		   b.setId(Utils.generateViewId());
		   b.setText(c.toString().toUpperCase(Locale.US));

		   LinearLayout.LayoutParams params = 
				   new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 
						   LinearLayout.LayoutParams.WRAP_CONTENT);

		   b.setLayoutParams(params);
		   ll.addView(b);
		   playerTileButtons.add(b);
	   }
   }
   
   private void setPlayerTileListeners() {
	   for (int i = 0; i < BananagramsLetters.sortedPlayerLettersList().size(); i++) {
		   final char t = BananagramsLetters.sortedPlayerLettersList().get(i);
		   playerTileButtons.get(i).setOnClickListener(new View.OnClickListener(){
			   public void onClick(View v) {
				   returnPlayerTileResult(t);
				   updatePlayerTileButtons();
			   }});
	   }
   }
   
   private void updatePlayerTileViews(Character c) {
	   BananagramsLetters.useLetterSinglePlayer(c);
	   GridAdapter.tiles.get(boardPosition).setSelected(false);
	   boardPosition = -1;

	   updatePlayerTileButtons();
   }

   /** Return the chosen tile to the caller */
   private void returnPlayerTileResult(char tile) {
	   if (boardPosition != -1) {
		   if (GridAdapter.tiles.get(boardPosition).getChar() != ' ') {
			   BananagramsLetters.returnLetter(GridAdapter.tiles.get(boardPosition).getChar());
		   }
		   GridAdapter.tiles.get(boardPosition).setChar(tile);
		   updateGrid();
		   updatePlayerTileViews(tile);
		   Music.play(this, R.raw.bananagrams_click, false);
	   }
   }
   
   private void determineTimeDifficulty(int diff) {
	   switch (diff) {
	   case DIFFICULTY_HARD:
		   timeRemaining = 3 * 60 * 1000;
		   break;
	   case DIFFICULTY_MEDIUM:
		   timeRemaining = 5 * 60 * 1000;
		   break;
	   case DIFFICULTY_EASY:
	   default:
		   timeRemaining = MAX_TIME;
		   break;
	   }
   }

   private void gameOver() {
	   GridView grid = (GridView)findViewById(R.id.bananagrams_board);
	   grid.setEnabled(false);
	   String message = "Game over! You scored " + pointsScored + " points with " + 
			   Math.floor(timeRemaining / 1000) + " seconds to spare!";
	   Toast.makeText(this, message, Toast.LENGTH_LONG).show();
   }
   
   @Override
   public void onClick(View v) {
	   switch (v.getId()) {
	   case R.id.bananagrams_pause_button:
		   Intent pauseIntent = new Intent(this, PauseMenu.class);
		   startActivity(pauseIntent);
		   break;
	   case R.id.bananagrams_save_game_button:
		   onPause();
		   finish();
		   break;
	   case R.id.bananagrams_finish_button:
		   // Game over!
		   SharedPreferences sets = getSharedPreferences(BANANA_PREFS, 0);
		   sets.edit().putBoolean(CONTINUE_GAME, false).apply();
		   timer.cancel();
		   gameOver();
		   finish();
		   break;
	   }
   }

   public void onToggleReturn(View view) {
	   Log.v(TAG, "toggle undo");
	   undo = ((ToggleButton) view).isChecked();
   }

}
