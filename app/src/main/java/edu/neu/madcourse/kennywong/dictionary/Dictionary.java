/***
 * Excerpted from "Hello, Android",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material, 
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose. 
 * Visit http://www.pragmaticprogrammer.com/titles/eband3 for more book information.
 ***/
package edu.neu.madcourse.kennywong.dictionary;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import edu.neu.madcourse.kennywong.R;
import edu.neu.madcourse.kennywong.utils.Wordlist;

public class Dictionary extends Activity implements OnClickListener {
	private static final String RESULTS_LIST = "dictionary_results";
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dictionary);

		// Set up click listeners for all the buttons
		View clearButton = findViewById(R.id.dictionary_clear_button);
		clearButton.setOnClickListener(this);
		View acknowledgementsButton = findViewById(R.id.dictionary_acknowledgements_button);
		acknowledgementsButton.setOnClickListener(this);
		View returnButton = findViewById(R.id.dictionary_return_button);
		returnButton.setOnClickListener(this);
		
		EditText inputField = (EditText)findViewById(R.id.dictionary_input_field);
		TextView outputField = (TextView)findViewById(R.id.dictionary_output_field);
		outputField.setMovementMethod(ScrollingMovementMethod.getInstance());
		DictionaryTextWatcher textWatcher = new DictionaryTextWatcher(outputField, this);
		inputField.addTextChangedListener(textWatcher);
		
		Wordlist.tryLoadResources(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();

		Music.stop(this);
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
	    super.onSaveInstanceState(outState);
	    
		TextView outputField = (TextView)findViewById(R.id.dictionary_output_field);
		CharSequence text = outputField.getText();
	    outState.putCharSequence(RESULTS_LIST, text);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
	    super.onRestoreInstanceState(savedInstanceState);
	    
	    TextView outputField = (TextView)findViewById(R.id.dictionary_output_field);
	    CharSequence results = savedInstanceState.getCharSequence(RESULTS_LIST);
	    outputField.setText(results);
	}
	
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.dictionary_clear_button:
			TextView outputField = (TextView)findViewById(R.id.dictionary_output_field);
			EditText inputField = (EditText)findViewById(R.id.dictionary_input_field);
			outputField.setText("");
			inputField.getText().clear();
			Wordlist.clearKnownWords();
			break;
		case R.id.dictionary_acknowledgements_button:
			Intent acknowledgementsIntent = new Intent(this, Acknowledgements.class);
			startActivity(acknowledgementsIntent);
			break;
		case R.id.dictionary_return_button:
			Wordlist.clearKnownWords();
			finish();
			break;
		}
	}

}