package edu.neu.madcourse.kennywong.runmania.bluetooth;

public class BluetoothConstants {
	public static final String EXTRAS_DEVICE_NAME = "edu.neu.madcourse.kennywong.DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS = "edu.neu.madcourse.kennywong.DEVICE_ADDRESS";
    public static final String EXTRAS_DEVICE_LIST = "edu.neu.madcourse.kennywong.DEVICES";
    
    public static final String HEART_RATE_SERVICE = "0000180d-0000-1000-8000-00805f9b34fb";
    public static final String DEVICE_INFO_SERVICE = "0000180a-0000-1000-8000-00805f9b34fb";
    
	public static final String CLIENT_CHARACTERISTIC_CONFIG = "00002902-0000-1000-8000-00805f9b34fb";
	public static final String HEART_RATE_MEASUREMENT = "00002a37-0000-1000-8000-00805f9b34fb";
	public static final String MANUFACTURER_NAME = "00002a29-0000-1000-8000-00805f9b34fb";
}
