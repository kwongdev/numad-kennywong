package edu.neu.madcourse.kennywong.runmania;

import android.app.ListActivity;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;

import edu.neu.madcourse.kennywong.R;
import edu.neu.madcourse.kennywong.runmania.bluetooth.BluetoothConstants;
import edu.neu.madcourse.kennywong.runmania.bluetooth.LeDeviceListAdapter;

public class SelectDevice extends ListActivity {
	public void onCreate(Bundle bund) {
		super.onCreate(bund);
		setTitle(R.string.title_devices);
		ArrayList<BluetoothDevice> devices = getIntent().getParcelableArrayListExtra(BluetoothConstants.EXTRAS_DEVICE_LIST);

		LeDeviceListAdapter adapter = new LeDeviceListAdapter(SelectDevice.this, devices);
		setListAdapter(adapter);
	}
	
	protected void onListItemClick(ListView l, View v, int position, long id) {
        final BluetoothDevice device = (BluetoothDevice)getListAdapter().getItem(position);
        if (device == null) return;
        final Intent intent = new Intent(this, EnterAge.class);
        intent.putExtra(BluetoothConstants.EXTRAS_DEVICE_NAME, device.getName());
        intent.putExtra(BluetoothConstants.EXTRAS_DEVICE_ADDRESS, device.getAddress());
        startActivity(intent);
    }
}
