package edu.neu.madcourse.kennywong.bananagrams.data;

import java.util.Locale;

import android.util.Log;
import edu.neu.madcourse.kennywong.bananagrams.GridAdapter;
import edu.neu.madcourse.kennywong.utils.Wordlist;

public class BananagramsCheck {
	private static final String TAG = "BananagramsCheck";
	
	private static int getX(int position) {
		return position % GridAdapter.COLUMNS;
	}
	
	private static int getY(int position) {
		return (int)Math.ceil(position / GridAdapter.COLUMNS);
	}
	
	public static BananagramsTile getTile(int x, int y) {
		return GridAdapter.tiles.get(GridAdapter.COLUMNS*y + x);
	}
	
	public static int XYtoPos(int x, int y) {
		return GridAdapter.COLUMNS*y + x;
	}
	
	// Assumption: startPosition is not empty
	private static void validateHorizontal(int startPosition) {
		int tileX = getX(startPosition);
		int tileY = getY(startPosition);
		
		// Determine potential word
		String potentialWord = "";
		int col = tileX;
		while((col < GridAdapter.COLUMNS) && getTile(col, tileY).getChar() != ' ') {
			potentialWord += getTile(col, tileY).getChar();
			col++;
		}
		
		boolean isEmptyToLeft = (tileX == 0) || getTile(tileX - 1, tileY).getChar() == ' ';
		Log.v(TAG, "Potential word is " + potentialWord);
		if (isEmptyToLeft && Wordlist.isValidNoCache(potentialWord.toLowerCase(Locale.US))) {
			Log.v(TAG, "Validating letter " + GridAdapter.tiles.get(startPosition).getChar());
			GridAdapter.tiles.get(startPosition).setStartOfHoriWord(true);
			setTilePaint(startPosition, true, true);
		} else {
			GridAdapter.tiles.get(startPosition).setStartOfHoriWord(false);
			Log.v(TAG, "Invalidating letter " + GridAdapter.tiles.get(startPosition).getChar());
			// if tile to left is unpainted, unpaint this one
			if (!hasValidWordLeft(startPosition)) {
				setTilePaint(startPosition, true, false);
				Log.v(TAG, "unpaint from " + startPosition + " for " + potentialWord.length());
			}
		}
	}
	
	private static void validateVertical(int startPosition) {
		int tileX = getX(startPosition);
		int tileY = getY(startPosition);
		
		// Determine potential word
		String potentialWord = "";
		int row = tileY;
		while((row < GridAdapter.getNumRows()) && getTile(tileX, row).getChar() != ' ') {
			potentialWord += getTile(tileX, row).getChar();
			row++;
		}
		
		boolean isEmptyAbove = (tileY == 0) || getTile(tileX, tileY - 1).getChar() == ' ';
		Log.v(TAG, "Potential word is " + potentialWord);
		if (isEmptyAbove && Wordlist.isValidNoCache(potentialWord.toLowerCase(Locale.US))) {
			Log.v(TAG, "Validating letter " + GridAdapter.tiles.get(startPosition).getChar());
			GridAdapter.tiles.get(startPosition).setStartOfVertWord(true);
			setTilePaint(startPosition, false, true);
		} else { // not empty above or not a word
			GridAdapter.tiles.get(startPosition).setStartOfVertWord(false);
			Log.v(TAG, "Invalidating letter " + GridAdapter.tiles.get(startPosition).getChar());
			// if tile above is unpainted, unpaint this one
			if (!hasValidWordAbove(startPosition)) { // only unpaint if nothing else needs the words painted
				setTilePaint(startPosition, false, false);
				Log.v(TAG, "unpaint from " + startPosition + " for " + potentialWord.length());
			}
		}
	}
	
	private static boolean hasValidWordAbove(int position) {
		int tileX = getX(position);
		int tileY = getY(position);
		
		int row = tileY - 1;
		boolean valid = false;
		while (row >= 0) {
			BananagramsTile tileAbove = GridAdapter.tiles.get(XYtoPos(tileX, row));
			if (tileAbove.getChar() == ' ') {
				return valid;
			}
			valid = tileAbove.isStartOfVertWord();
			row--;
		}
		
		return valid;
	}
	
	private static boolean hasValidWordLeft(int position) {
		int tileX = getX(position);
		int tileY = getY(position);
		
		int col = tileX - 1;
		boolean valid = false;
		while (col >= 0) {
			BananagramsTile tileLeft = GridAdapter.tiles.get(XYtoPos(col, tileY));
			if (tileLeft.getChar() == ' ') {
				return valid;
			}
			valid = tileLeft.isStartOfHoriWord();
			col--;
		}
		
		return valid;
	}
	
	private static void setTilePaint(int position, boolean horizontal, boolean paint) {
		int posX = getX(position);
		int posY = getY(position);
		int col = posX;
		int row = posY;
		
		if (horizontal) {
			while (col < GridAdapter.COLUMNS && GridAdapter.tiles.get(XYtoPos(col, posY)).getChar() != ' ') {
				GridAdapter.tiles.get(XYtoPos(col, posY)).setHoriPaint(paint);
				col++;
			}
		} else {
			while (row < GridAdapter.getNumRows() && GridAdapter.tiles.get(XYtoPos(posX, row)).getChar() != ' ') {
				GridAdapter.tiles.get(XYtoPos(posX, row)).setVertPaint(paint);
				row++;
			}
		}
		
	}
	
	public static void validate(int position) {
		// Ensure blank tiles are not painted
		if (GridAdapter.tiles.get(position).getChar() != ' ') {
			validateHorizontal(position);
			validateVertical(position);
		}
	}
}
