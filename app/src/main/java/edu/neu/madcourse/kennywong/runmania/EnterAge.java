package edu.neu.madcourse.kennywong.runmania;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.NumberPicker;
import android.widget.NumberPicker.Formatter;
import android.widget.NumberPicker.OnValueChangeListener;

import edu.neu.madcourse.kennywong.R;

public class EnterAge extends Activity implements OnValueChangeListener,
		Formatter, OnClickListener {
	private static final String TAG = "EnterAge";
	private int passedAge;
	private NumberPicker numberPicker;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.runmania_enterage);
		
		SharedPreferences settings = getSharedPreferences(RunmaniaConstants.SHARED_PREFS, 0);
		int savedAge = settings.getInt(RunmaniaConstants.SAVED_AGE, 20);

		numberPicker = (NumberPicker) findViewById(R.id.runmania_age_picker);
		numberPicker.setFormatter(this);
		numberPicker.setOnValueChangedListener(this);
		numberPicker.setMaxValue(100);
		numberPicker.setMinValue(1);
		numberPicker.setValue(savedAge);
		numberPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

		View nextButton = findViewById(R.id.runmania_enterage_next);
		nextButton.setOnClickListener(this);

		View quitButton = findViewById(R.id.runmania_enterage_quit);
		quitButton.setOnClickListener(this);
		
		
	}

	public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
		Log.d(TAG, "value changed from " + "oldVald to " + newVal);
	}

	public String format(int value) {
		String tmpStr = String.valueOf(value);
		if (value < 10) {
			tmpStr = "0" + tmpStr;
		}
		return tmpStr;

	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.runmania_enterage_next:
			Intent intensityIntent = new Intent(this, EnterIntensity.class);
			passedAge = numberPicker.getValue();
			
			SharedPreferences settings = getSharedPreferences(RunmaniaConstants.SHARED_PREFS, 0);
			settings.edit().putInt(RunmaniaConstants.SAVED_AGE, numberPicker.getValue()).apply();
			
			intensityIntent.putExtra(RunmaniaConstants.AGE, passedAge);
			intensityIntent.putExtras(getIntent());
			startActivity(intensityIntent);
			break;
		case R.id.runmania_enterage_quit:
			Intent intent = new Intent(getApplicationContext(), RunmaniaMenu.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			break;
		}
	}

}
