/***
 * Excerpted from "Hello, Android",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material, 
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose. 
 * Visit http://www.pragmaticprogrammer.com/titles/eband3 for more book information.
***/
package edu.neu.madcourse.kennywong.mpwordgame;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import edu.neu.madcourse.kennywong.R;

public class TopScores extends Activity {
	
	static String scores = "";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.multiplayer_top_scores);
		
		TextView topScoresContent = (TextView)findViewById(R.id.multiplayer_top_scores_content);
		topScoresContent.setText(scores);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		TextView topScoresContent = (TextView)findViewById(R.id.multiplayer_top_scores_content);
		topScoresContent.setText(scores);
	}
}
