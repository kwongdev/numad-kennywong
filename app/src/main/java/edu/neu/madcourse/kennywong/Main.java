/***
 * Excerpted from "Hello, Android",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material, 
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose. 
 * Visit http://www.pragmaticprogrammer.com/titles/eband3 for more book information.
 ***/
package edu.neu.madcourse.kennywong;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import edu.neu.madcourse.kennywong.bananagrams.BananagramsMenu;
import edu.neu.madcourse.kennywong.communication.CommunicationMenu;
import edu.neu.madcourse.kennywong.dictionary.Dictionary;
import edu.neu.madcourse.kennywong.mpwordgame.WordGameMenu;
import edu.neu.madcourse.kennywong.runmania.RunmaniaDescription;
import edu.neu.madcourse.kennywong.runmania.TrickiestPart;
import edu.neu.madcourse.kennywong.sudoku.Sudoku;

public class Main extends Activity implements OnClickListener {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		setTitle(R.string.titlebar);

		// Set up click listeners for all the buttons
		View sudokuButton = findViewById(R.id.sudoku_button);
		sudokuButton.setOnClickListener(this);
		View dictionaryButton = findViewById(R.id.dictionary_button);
		dictionaryButton.setOnClickListener(this);
		View bananagramsButton = findViewById(R.id.bananagrams_button);
		bananagramsButton.setOnClickListener(this);
		View communicationButton = findViewById(R.id.communication_button);
		communicationButton.setOnClickListener(this);
		View multiplayerButton = findViewById(R.id.multiplayer_button);
		multiplayerButton.setOnClickListener(this);
		View trickiestPartButton = findViewById(R.id.trickiest_part_button);
		trickiestPartButton.setOnClickListener(this);
		View finalProjectButton = findViewById(R.id.final_project_button);
		finalProjectButton.setOnClickListener(this);
		View generateErrorButton = findViewById(R.id.generate_error_button);
		generateErrorButton.setOnClickListener(this);
		View aboutButton = findViewById(R.id.about_button);
		aboutButton.setOnClickListener(this);
		View exitButton = findViewById(R.id.exit_button);
		exitButton.setOnClickListener(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.sudoku_button:
			Intent sudokuIntent = new Intent(this, Sudoku.class);
			startActivity(sudokuIntent);
			break;
		case R.id.dictionary_button:
			Intent dictionaryIntent = new Intent(this, Dictionary.class);
			startActivity(dictionaryIntent);
			break;
		case R.id.bananagrams_button:
			Intent bananagramsIntent = new Intent(this, BananagramsMenu.class);
			startActivity(bananagramsIntent);
			break;
		case R.id.communication_button:
			Intent communicationIntent = new Intent(this, CommunicationMenu.class);
			startActivity(communicationIntent);
			break;
		case R.id.multiplayer_button:
			Intent multiplayerIntent = new Intent(this, WordGameMenu.class);
			startActivity(multiplayerIntent);
			break;
		case R.id.trickiest_part_button:
			Intent trickiestPartIntent = new Intent(this, TrickiestPart.class);
			startActivity(trickiestPartIntent);
			break;
		case R.id.final_project_button:
			Intent finalProjectIntent = new Intent(this, RunmaniaDescription.class);
			startActivity(finalProjectIntent);
			break;
		case R.id.about_button:
			Intent aboutIntent = new Intent(this, About.class);
			startActivity(aboutIntent);
			break;
		case R.id.generate_error_button:
			@SuppressWarnings("unused") // Actually used to cause an exception
			int expectedError = 1/0;
			break;
		case R.id.exit_button:
			finish();
			break;
		}
	}

}