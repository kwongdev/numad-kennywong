package edu.neu.madcourse.kennywong.runmania;

public class RunmaniaConstants {
	// INTENTS
	public static final String SONG_PATHS = "edu.neu.madcourse.kennywong.SONG_PATHS";
	public static final String HEART_RATE = "edu.neu.madcourse.kennywong.HEART_RATE";
	public static final String AGE = "edu.neu.madcourse.kennywong.AGE";
	public static final String LOW_HR = "edu.neu.madcourse.kennywong.LOW_HR";
	public static final String ON_HR = "edu.neu.madcourse.kennywong.ON_HR";
	public static final String HIGH_HR = "edu.neu.madcourse.kennywong.HIGH_HR";
	
	// SHARED_PREFERENCES
	public static final String SHARED_PREFS = "edu.neu.madcourse.kennywong.RunmaniaPrefs";
	public static final String SAVED_AGE = "Age";
    public static final String SAVED_INTENSITY = "Intensity";
//	public static final String SAVED_PATH = "Path"; // TODO: Save the path that was used
	public static final String SAVED_MUSIC = "SongPaths";
}
