package edu.neu.madcourse.kennywong.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.util.HashMap;
import java.util.HashSet;

import android.content.Context;
import edu.neu.madcourse.kennywong.R;

public class Wordlist {
	private static final String RESOURCE_FILE = "wordlist.bin";
	
	// Assumptions: this structure won't get too big since the app won't accumulate that many words
	private static HashSet<String> knownWords = new HashSet<String>();
	
	private static int headerSize;
	private static HashMap<LetterDetails, WordOffsets> groupStartOffsets = new HashMap<LetterDetails, WordOffsets>();
	
	public static File wordListFile;

	public static boolean isValidNoCache(String candidate) {
		return inWordList(candidate);
	}
	
	public static boolean isValid(String candidate) {
		if (!knownWords.contains(candidate) && inWordList(candidate)) {
			knownWords.add(candidate);
			return true;
		}
		return false;
	}

	public static void clearKnownWords() {
		knownWords.clear();
	}
	
	/** Tries to load the wordlist resource; does nothing if it's already loaded **/
	public static void tryLoadResources(Context c) {
		// Store "temp" version in app directory
		File dir = c.getCacheDir();
		dir.mkdirs();
		Wordlist.wordListFile = new File(dir, RESOURCE_FILE);
		
		if (Wordlist.wordListFile.exists()) {
			return;
		}
		
		InputStream in = c.getResources().openRawResource(R.raw.wordlist);
	    FileOutputStream fos = null;
	    try {
	        // Write the asset file to the temporary location
	        fos = new FileOutputStream(Wordlist.wordListFile);
	        byte[] buffer = new byte[1024];
	        int bufferLen;
	        while ((bufferLen = in.read(buffer)) != -1) {
	            fos.write(buffer, 0, bufferLen);
	        }
	    } catch (FileNotFoundException fne) {
	    	fne.printStackTrace();
	    } catch (IOException ioe) {
			ioe.printStackTrace();
		}
	    finally {
	    	if (fos != null) {
	    		try {
	    			fos.close();
	    		} catch (IOException e) {}
	    	}
	    }
	}
	
	private static boolean inWordList(String candidate) {
		RandomAccessFile raf = null;
		
		if (candidate.length() < 1) {
			return false;
		}
		
		try {
			raf = new RandomAccessFile(Wordlist.wordListFile, "r");
			
			processHeader(raf);
			
			LetterDetails binDetails = new LetterDetails(candidate.charAt(0), candidate.length());
			WordOffsets offsets = groupStartOffsets.get(binDetails);
			if (offsets == null) {
				return false;
			}
			int bytesToDataStart = offsets.getBytesToDataStart();
			int lengthOfGroup = offsets.getLengthOfGroup();
			
			// Start at "words starting with ??? of length ???"
			raf.seek(bytesToDataStart);
			// Get length of group in bytes
			int groupEndpoint = bytesToDataStart + lengthOfGroup;

			int candidateHash = candidate.hashCode();

			do {
				raf.readInt();
//				int possibleHash = raf.readInt(); //There may be a bug regarding the generation of this...
				int wordLen = raf.readByte();
				
				byte[] wordBytes = new byte[wordLen];
				raf.read(wordBytes, 0, wordLen);
				String possibleString = new String(wordBytes);
				
				if (candidateHash == possibleString.hashCode() && candidate.equals(possibleString)) {
					return true;
				}
			} while (raf.getFilePointer() <= groupEndpoint);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				raf.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return false;
	}
	
	private static void processHeader(RandomAccessFile raf) {
		try {
			raf.seek(0);
			
			if (headerSize == 0) {
				headerSize = raf.readInt();
			}
			
			if (groupStartOffsets.isEmpty()) {
				for(int i = Integer.SIZE / 8; i < headerSize; i = (int) raf.getFilePointer()) {
					char letter = (char)raf.readByte();
					int wordLength = raf.readByte();
					int bytesToDataStart = raf.readInt();
					int lengthOfGroup = raf.readInt();
					
					LetterDetails key = new LetterDetails(letter, wordLength);
					WordOffsets value = new WordOffsets(bytesToDataStart, lengthOfGroup);
					
					groupStartOffsets.put(key, value);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	

}
