/**
 * Credits to: Bill Cox for the sample code for libsonic (see Acknowledgements)
 */

package edu.neu.madcourse.kennywong.runmania;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaCodec;
import android.media.MediaCodec.BufferInfo;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.media.MediaMetadataRetriever;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Toast;

import org.vinuxproject.sonic.Sonic;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import edu.neu.madcourse.kennywong.R;
import edu.neu.madcourse.kennywong.runmania.util.AndroidAudioDevice;
import edu.neu.madcourse.kennywong.runmania.util.SimpleFileDialog;

public class TrickiestPart extends Activity {
	private static final String TAG = "TrickiestPart";
	public static int samplingFrequency = 44100;
	public static int channelCount = 2;
	private float speed = 1.0f;
	private Decoder decoder = null;
	private Player player = null;
	
	private static final int TIMEOUT = 200;
	private static final TimeUnit TIMEOUT_UNIT = TimeUnit.MILLISECONDS;
	
	String m_chosen;
	
	LinkedBlockingQueue<byte[]> byteQueue = new LinkedBlockingQueue<byte[]>();
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.trickiest_part);
		Log.v(TAG, "onCreate");

		final SeekBar sk = (SeekBar) findViewById(R.id.speedBar);     
		sk.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override       
			public void onStopTrackingTouch(SeekBar seekBar) { }       

			@Override       
			public void onStartTrackingTouch(SeekBar seekBar) { }       

			@Override       
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				speed = progress / 50f;
				Log.v(TAG, "speed: " + speed);
			}       
		});
		
	}
	
	public void browse(View view) {
		SimpleFileDialog FileOpenDialog =  new SimpleFileDialog(TrickiestPart.this,
				Environment.getExternalStorageDirectory().getAbsolutePath(), new SimpleFileDialog.SimpleFileDialogListener()
		{
			@Override
			public void onChosenDir(String chosenDir) 
			{
				// The code in this function will be executed when the dialog OK button is pushed 
				m_chosen = chosenDir;
				
				MediaMetadataRetriever mmr = new MediaMetadataRetriever();
				mmr.setDataSource(chosenDir);

				String title =
				     mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
				
				Toast.makeText(TrickiestPart.this, "Song name is: " + title, Toast.LENGTH_SHORT).show();
				
				if (decoder == null) {
					decoder = new Decoder(byteQueue);
					Log.v(TAG, "starting decoding");
					decoder.start();
				}
			}
		});

		//You can change the default filename using the public variable "Default_File_Name"
		FileOpenDialog.Default_File_Name = "";
		FileOpenDialog.chooseFile_or_Dir();
	}
    
    public void play(View view) {
    	player = new Player(byteQueue);
    	player.start();
//    	else if (player != null && !player.isPlaying) {
//    		player.resumeMusic();
//    	}
    }
    
//    public void pause(View view) {
//    	if (player != null && player.isPlaying) {
//    		player.pause();
//    		// pause
//    	}
//    }
    
    public void stop(View view) {
    	if (player != null) {
    		player.stopMusic();
    	}
    }
    
    public void acknowledgements(View view) {
    	Intent ackIntent = new Intent(this, Acknowledgements.class);
		startActivity(ackIntent);
    }
    
    public void exit(View view) {
    	stop(view);
    	finish();
    }
    
    // Consumer class
    private class Player extends Thread {
//    	private static final String TAG = "Player";
    	private final LinkedBlockingQueue<byte[]> queue;
    	
    	private AndroidAudioDevice device;
    	
    	public Player(LinkedBlockingQueue<byte[]> q) {
    		this.queue = q;
    	}
    	
    	@Override
    	public void run() {
    		device = new AndroidAudioDevice(samplingFrequency, channelCount);
            Sonic sonic = new Sonic(samplingFrequency, channelCount);
            byte samples[] = new byte[4096];
            byte modifiedSamples[] = new byte[2048];
            
			try {
				InputStream pcmStream;
				byte[] data;
				
				device.play();
				
				while ((data = queue.poll(TIMEOUT, TIMEOUT_UNIT)) != null) {
					pcmStream = new ByteArrayInputStream(data);
					for (int bytesRead = pcmStream.read(samples); bytesRead >= 0; bytesRead = pcmStream.read(samples)) {
			    		sonic.setSpeed(speed);
			    		
			    		if(bytesRead > 0) {
				        	sonic.putBytes(samples, bytesRead);
				        } else {
						    sonic.flush();
				        }
			        	int available = sonic.availableBytes(); 
			        	if(available > 0) {
			        		if(modifiedSamples.length < available) {
			        		    modifiedSamples = new byte[available*2];
			        		}
			        		sonic.receiveBytes(modifiedSamples, available);
			        		device.writeSamples(modifiedSamples, available);
			        	}
				    }
//					device.flush();
				}
				
				device.flush();
			} catch (InterruptedException ie) {
				ie.printStackTrace();
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
        }
    	
//    	public void resumeMusic() {
//    		device.play();
//    		isPlaying = true;
//    	}
    	
//    	public void pause() {
//    		device.pause();
//    		isPlaying = false;
//    	}
    	
    	public void stopMusic() {
    		device.stop();
    		queue.clear();
    	}
    	
    }

    // Producer class
	private class Decoder extends Thread {
		private static final String TAG = "Decoder";
		private MediaExtractor extractor;
		private MediaCodec decoder;
		private final LinkedBlockingQueue<byte[]> queue;

		public Decoder(LinkedBlockingQueue<byte[]> q) {
			this.queue = q;
		}

		@Override
		public void run() {
			extractor = new MediaExtractor();
			
			try {
				if (m_chosen != null) {
					extractor.setDataSource(m_chosen);
				}
			} catch (IOException e) {
				e.printStackTrace(); // could not set data source
			}

			for (int i = 0; i < extractor.getTrackCount(); i++) {
				MediaFormat format = extractor.getTrackFormat(i);
				samplingFrequency = format.getInteger(MediaFormat.KEY_SAMPLE_RATE); // assumes these values
				channelCount = format.getInteger(MediaFormat.KEY_CHANNEL_COUNT);    // don't change in a file
				String mime = format.getString(MediaFormat.KEY_MIME);
				if (mime.startsWith("audio/")) {
					extractor.selectTrack(i);
                    try {
                        decoder = MediaCodec.createDecoderByType(mime);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    decoder.configure(format, null, null, 0);
					break;
				}
			}

			if (decoder == null) {
				Log.e(TAG, "Can't find audio info!");
				return;
			}

			decoder.start();

			ByteBuffer[] inputBuffers = decoder.getInputBuffers();
			ByteBuffer[] outputBuffers = decoder.getOutputBuffers();
			BufferInfo info = new BufferInfo();
			boolean isEOS = false;

			while (true) {
			    if (!isEOS) {
			        int inIndex = decoder.dequeueInputBuffer(10000);
			        if (inIndex >= 0) {
			            ByteBuffer buffer = inputBuffers[inIndex];
			            int sampleSize = extractor.readSampleData(buffer, 0);
			            if (sampleSize < 0) {
			                // We shouldn't stop the playback at this point, just pass the EOS
			                // flag to decoder, we will get it again from the dequeueOutputBuffer
			                Log.d(TAG, "InputBuffer BUFFER_FLAG_END_OF_STREAM");
			                decoder.queueInputBuffer(inIndex, 0, 0, 0, MediaCodec.BUFFER_FLAG_END_OF_STREAM);
			                isEOS = true;
			            } else {
			                decoder.queueInputBuffer(inIndex, 0, sampleSize, extractor.getSampleTime(), 0);
			                extractor.advance();
			            }
			        }
			    }

			    int outIndex = decoder.dequeueOutputBuffer(info, 10000);
			    switch (outIndex) {
			    case MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED:
			        Log.d(TAG, "INFO_OUTPUT_BUFFERS_CHANGED");
			        outputBuffers = decoder.getOutputBuffers();
			        break;
			    case MediaCodec.INFO_OUTPUT_FORMAT_CHANGED:
			        Log.d(TAG, "New format " + decoder.getOutputFormat());
			        break;
			    case MediaCodec.INFO_TRY_AGAIN_LATER:
			        Log.d(TAG, "dequeueOutputBuffer timed out!");
			        break;
			    default:
			        ByteBuffer buffer = outputBuffers[outIndex];
			        byte[] b = new byte[info.size-info.offset];                         
			        int a = buffer.position();
			        buffer.get(b);
			        buffer.position(a);

			        queue.offer(b);
			        
			        decoder.releaseOutputBuffer(outIndex, true);
			        break;
			    }

			    // All decoded frames have been rendered, we can stop playing now
			    if ((info.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
			        Log.d(TAG, "OutputBuffer BUFFER_FLAG_END_OF_STREAM");
			        break;
			    }
			}

			decoder.stop();
			decoder.release();
			extractor.release();
		}
	}
}
