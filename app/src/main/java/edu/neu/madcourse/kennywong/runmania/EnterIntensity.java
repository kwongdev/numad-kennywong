package edu.neu.madcourse.kennywong.runmania;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RadioGroup;
import android.widget.Toast;

import edu.neu.madcourse.kennywong.R;
import edu.neu.madcourse.kennywong.runmania.bluetooth.BluetoothLeService;

public class EnterIntensity extends Activity implements OnClickListener {
	private static final String TAG = "EnterIntensity";
	private int passedIntensity = 0;
    private static final int INTENSE_INTENSITY = 85;
    private static final int MODERATE_INTENSITY = 70;
    private static final int LIGHT_INTENSITY = 50;
	private int passedAge;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.runmania_enterintensity);

		Intent ageIntent = getIntent();
		passedAge = ageIntent.getIntExtra(RunmaniaConstants.AGE, 0);

		View backButton = findViewById(R.id.runmania_enterintensity_back);
		backButton.setOnClickListener(this);

		View nextButton = findViewById(R.id.runmania_enterintensity_next);
		nextButton.setOnClickListener(this);

		View quitButton = findViewById(R.id.runmania_enterintensity_quit);
		quitButton.setOnClickListener(this);

        SharedPreferences settings = getSharedPreferences(RunmaniaConstants.SHARED_PREFS, 0);
        Integer selectedRadio = settings.getInt(RunmaniaConstants.SAVED_INTENSITY, -1);

		RadioGroup radioGroup = (RadioGroup) findViewById(R.id.runmania_radio_group);
		radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.runmania_intense:
                        passedIntensity = INTENSE_INTENSITY;
                        break;
                    case R.id.runmania_moderate:
                        passedIntensity = MODERATE_INTENSITY;
                        break;
                    case R.id.runmania_light:
                        passedIntensity = LIGHT_INTENSITY;
                        break;
                }

                SharedPreferences settings = getSharedPreferences(RunmaniaConstants.SHARED_PREFS, 0);
                settings.edit().putInt(RunmaniaConstants.SAVED_INTENSITY, checkedId).apply();
			}
		});

        radioGroup.check(selectedRadio);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.runmania_enterintensity_next:
			if (passedIntensity == 0) {
				Toast.makeText(this, "Please select an intensity first!",
						Toast.LENGTH_LONG).show();
				break;
			}
			Intent musicIntent = new Intent(this, EnterMusic.class);
			
			//calculate HR here
			int target_hr = (int)((220 - passedAge) * (passedIntensity / 100.0));
			musicIntent.putExtra(RunmaniaConstants.HEART_RATE, target_hr);
			musicIntent.putExtras(getIntent());
			BluetoothLeService.target_heartrate = target_hr;
			startActivity(musicIntent);
			break;
		case R.id.runmania_enterintensity_back:
			finish();
			break;
		case R.id.runmania_enterintensity_quit:
			Intent intent = new Intent(getApplicationContext(), RunmaniaMenu.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			break;
		}
	}

}
