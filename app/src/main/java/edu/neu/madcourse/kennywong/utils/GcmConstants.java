package edu.neu.madcourse.kennywong.utils;

public class GcmConstants
{
    public static final String TAG = "GCM_Globals";
    public static final String GCM_SENDER_ID = "430655361424";
    public static final String BASE_URL = "https://android.googleapis.com/gcm/send";
    public static final String PREFS_NAME = "GCM_Communication";
    public static final String GCM_API_KEY = "AIzaSyBxsisWyo2ogAlkL0PmJDyQnan7ElackVs";
    public static final int SIMPLE_NOTIFICATION = 22;
    public static final long GCM_TIME_TO_LIVE = 60L * 60L * 24L * 7L * 4L; // 4 Weeks
    public static int mode = 0;
    
    public static final String MHEALTH_TEAM_NAME = "kwong";
    public static final String MHEALTH_PASS = "numad14f-kwong";
}