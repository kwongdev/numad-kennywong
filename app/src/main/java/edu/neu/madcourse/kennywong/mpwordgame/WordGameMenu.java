/***
 * Excerpted from "Hello, Android",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material, 
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose. 
 * Visit http://www.pragmaticprogrammer.com/titles/eband3 for more book information.
 ***/
package edu.neu.madcourse.kennywong.mpwordgame;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import edu.neu.madcourse.kennywong.R;
import edu.neu.madcourse.kennywong.bananagrams.Music;
import edu.neu.madcourse.kennywong.bananagrams.Prefs;
import edu.neu.madcourse.kennywong.bananagrams.SinglePlayerGame;
import edu.neu.madcourse.kennywong.communication.CommunicationNotification;
import edu.neu.madcourse.kennywong.communication.Help;
import edu.neu.madcourse.kennywong.utils.GcmConstants;
import edu.neu.madcourse.kennywong.utils.Wordlist;
import edu.neu.mhealth.api.KeyValueAPI;

public class WordGameMenu extends Activity implements OnClickListener {

	public static final String EXTRA_MESSAGE = "message";
	public static final String DEVICE_ID = "DEVICE_ID";
	public static final String PROPERTY_REG_ID = "registration_id";
	private static final String PROPERTY_APP_VERSION = "appVersion";
	public static String PROPERTY_ALERT_TEXT = "alertText";
	public static String PROPERTY_TITLE_TEXT = "titleText";
	public static String PROPERTY_CONTENT_TEXT = "contentText";
	public static final String PROPERTY_NTYPE = "nType";
	public static final String GAME_STATUS = "_GameStatus";
	public static final String GAME_BOARD = "_GameBoard";
	public static final String GAME_PLAYER = "_GamePlayer";
	public static final String GAME_BUNCH = "_GameBunch";
	public static final String TOP_SCORE = "_TopScore";
	
	private static int SIMUL_GAME_MODE = 0;
	private static int ASYNC_GAME_MODE = 1;
	
	public static final String ERROR_TAG = "Error";
	public static final String NO_INTERNET_ERROR = "Error: No internet connection detected. Multiplayer may not work!";
	public static boolean instantiated = false;
	
	private static final String TAG = "WordGame";

	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

	GoogleCloudMessaging gcm;
	SharedPreferences prefs;
	static Context context;
	static String regid;
	static String opponentRegid = "";
	boolean opponentInGame = false;
	boolean opponentFound = false;
	
	static int maybeHighScore = -1;
	
	// KEY = regid#, VALUE = regid{1,2,3,...}
	static HashMap<String,String> registeredDevices = new HashMap<String,String>();
	int numOfRegDevices = registeredDevices.size();

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.multiplayer_wordgame_menu);

		context = getApplicationContext();

		// Set up click listeners for all the buttons
		View newSpButton = findViewById(R.id.bananagrams_new_button);
		newSpButton.setOnClickListener(this);

		SharedPreferences spSettings = getSharedPreferences(SinglePlayerGame.BANANA_PREFS, 0);
		View continueSPButton = findViewById(R.id.wordgame_continue_sp_button);
		boolean continueSPGame = spSettings.getBoolean(SinglePlayerGame.CONTINUE_GAME, false);
		if (continueSPGame) {
			continueSPButton.setVisibility(View.VISIBLE);
			continueSPButton.setOnClickListener(this);
		} else {
			continueSPButton.setVisibility(View.GONE);
		}
		
		SharedPreferences mpSettingsAsync = getSharedPreferences(AsyncGame.BANANA_PREFS, 0);
		SharedPreferences mpSettingsSimul = getSharedPreferences(SimulGame.BANANA_PREFS, 0);
		
		View continueMPButton = findViewById(R.id.wordgame_continue_mp_button);
		boolean continueMPGame = mpSettingsAsync.getBoolean(AsyncGame.CONTINUE_GAME, false) ||
									mpSettingsSimul.getBoolean(SimulGame.CONTINUE_GAME, false);
		if (continueMPGame) {
			continueMPButton.setVisibility(View.VISIBLE);
			continueMPButton.setOnClickListener(this);
		} else {
			continueMPButton.setVisibility(View.GONE);
		}

		View findPlayerButton = findViewById(R.id.communication_find_player_button);
		findPlayerButton.setOnClickListener(this);
		View newMpButton = findViewById(R.id.communication_new_mp_button);
		newMpButton.setOnClickListener(this);
		View helpButton = findViewById(R.id.communication_help_button);
		helpButton.setOnClickListener(this);
		View scoresButton = findViewById(R.id.communication_top_score_button);
		scoresButton.setOnClickListener(this);
		View acknowledgementButton = findViewById(R.id.communication_acknowledgement_button);
		acknowledgementButton.setOnClickListener(this);
		View exitButton = findViewById(R.id.bananagrams_exit_button);
		exitButton.setOnClickListener(this);

		Wordlist.tryLoadResources(this);

		if (checkPlayServices()) {
			gcm = GoogleCloudMessaging.getInstance(this);
			regid = getRegistrationId(context);

			if (regid.isEmpty()) {
				registerInBackground();
			}
			
			updateNumOfRegDevices();
			instantiated = true;
		} else {
			Log.i(TAG, "No valid Google Play Services APK found.");
		}
	}

	/**
	 * Gets the current registration ID for application on GCM service.
	 * <p>
	 * If result is empty, the app needs to register.
	 *
	 * @return registration ID, or empty string if there is no existing
	 *         registration ID.
	 */
	private String getRegistrationId(Context context) {
	    final SharedPreferences prefs = getGCMPreferences(context);
	    String registrationId = prefs.getString(PROPERTY_REG_ID, "");
	    if (registrationId.isEmpty()) {
	        Log.i(TAG, "Registration not found.");
	        return "";
	    }
	    // Check if app was updated; if so, it must clear the registration ID
	    // since the existing regID is not guaranteed to work with the new
	    // app version.
	    int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
	    int currentVersion = getAppVersion(context);
	    if (registeredVersion != currentVersion) {
	        Log.i(TAG, "App version changed.");
	        return "";
	    }
	    return registrationId;
	}

	/**
	 * @return Application's {@code SharedPreferences}.
	 */
	private SharedPreferences getGCMPreferences(Context context) {
	    // This sample app persists the registration ID in shared preferences, but
	    // how you store the regID in your app is up to you.
	    return getSharedPreferences(WordGameMenu.class.getSimpleName(),
	            Context.MODE_PRIVATE);
	}

	/**
	 * @return Application's version code from the {@code PackageManager}.
	 */
	private static int getAppVersion(Context context) {
	    try {
	        PackageInfo packageInfo = context.getPackageManager()
	                .getPackageInfo(context.getPackageName(), 0);
	        return packageInfo.versionCode;
	    } catch (NameNotFoundException e) {
	        // should never happen
	        throw new RuntimeException("Could not get package name: " + e);
	    }
	}

	private void registerInBackground() {
		new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				String msg = "";
				try {
					if (gcm == null) {
						gcm = GoogleCloudMessaging.getInstance(context);
					}
					regid = gcm.register(GcmConstants.GCM_SENDER_ID);
					
					int cnt = 0;
					if (KeyValueAPI.isServerAvailable()) {
						String deviceCnt = KeyValueAPI.get(GcmConstants.MHEALTH_TEAM_NAME, 
															GcmConstants.MHEALTH_PASS,
															"cnt");
						if (!deviceCnt.contains(ERROR_TAG)) {
							Log.d("????", deviceCnt);
							cnt = Integer.parseInt(deviceCnt);
						}
						String getString;
						boolean flag = false;
						for (int i = 1; i <= cnt; i++) {
							getString = KeyValueAPI.get(GcmConstants.MHEALTH_TEAM_NAME, 
									  					GcmConstants.MHEALTH_PASS,
									  					"regid" + String.valueOf(i));
							Log.d("regid" + String.valueOf(i), getString);
							if (getString.equals(regid)) {
								flag = true; // device is already registered here before
								
								KeyValueAPI.put(GcmConstants.MHEALTH_TEAM_NAME, 
									    GcmConstants.MHEALTH_PASS,
									    "regid" + String.valueOf(cnt) + GAME_STATUS, 
									    "false");
							}
						}
						if (!flag) {
							KeyValueAPI.put(GcmConstants.MHEALTH_TEAM_NAME, 
									  		GcmConstants.MHEALTH_PASS,
									  		"cnt",
									  		String.valueOf(++cnt)); // increment num devices
							KeyValueAPI.put(GcmConstants.MHEALTH_TEAM_NAME, 
										    GcmConstants.MHEALTH_PASS,
										    "regid" + String.valueOf(cnt), 
										    regid); // register this device to MHEALTH
							KeyValueAPI.put(GcmConstants.MHEALTH_TEAM_NAME, 
								    GcmConstants.MHEALTH_PASS,
								    "regid" + String.valueOf(cnt) + GAME_STATUS, 
								    "false"); // set device to not in game
						}
						msg = "Device registered, registration index is " + "regid" + cnt;
					} else {
						msg = NO_INTERNET_ERROR;
						return msg;
					}
					sendRegistrationIdToBackend();
					storeRegistrationId(context, regid);
				} catch (IOException ex) {
					msg = "Error :" + ex.getMessage();
				}
				return msg;
			}

			@Override
			protected void onPostExecute(String msg) {
				Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
			}
		}.execute(null, null, null);
	}

	private void updateNumOfRegDevices() {
		new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				int cnt = 0;
				if (KeyValueAPI.isServerAvailable()) {
					String deviceCnt = KeyValueAPI.get(GcmConstants.MHEALTH_TEAM_NAME, 
														GcmConstants.MHEALTH_PASS,
														"cnt");
					if (!deviceCnt.contains(ERROR_TAG)) {
						cnt = Integer.parseInt(deviceCnt);
					}
					
					for (int i = 1; i <= cnt; i++) {
						registeredDevices.put(KeyValueAPI.get(GcmConstants.MHEALTH_TEAM_NAME, 
											  					GcmConstants.MHEALTH_PASS,
											  					"regid" + String.valueOf(i)), "regid" + String.valueOf(i));
					}
				}
				return String.valueOf(cnt);
			}

			@Override
			protected void onPostExecute(String msg) {
				Log.v(TAG, "Number of registered devices found: " + msg);
				if (!msg.contains(ERROR_TAG)) {
					numOfRegDevices = Integer.parseInt(msg);
				}
			}
		}.execute(null, null, null);
	}
	
	private void sendRegistrationIdToBackend() {
		// Your implementation here.
		// NO-OP
	}

	private void storeRegistrationId(Context context, String regId) {
		final SharedPreferences prefs = getGCMPreferences(context);
		int appVersion = getAppVersion(context);
		Log.i(TAG, "Saving regId on app version " + appVersion);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(PROPERTY_REG_ID, regId);
		editor.putInt(PROPERTY_APP_VERSION, appVersion);
		editor.commit();
	}

	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, this,
						PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				Log.i(TAG, "This device is not supported.");
				finish();
			}
			return false;
		}
		return true;
	}
	
	private void opponentIsInGame() {
		new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				if (KeyValueAPI.isServerAvailable()) {
					return KeyValueAPI.get(GcmConstants.MHEALTH_TEAM_NAME, 
										   GcmConstants.MHEALTH_PASS,
										   registeredDevices.get(opponentRegid) + GAME_STATUS);
				} else {
					return ERROR_TAG;
				}
			}

			@Override
			protected void onPostExecute(String msg) {
				Log.v(TAG, "opponent is in game? " + msg);
				opponentInGame = Boolean.parseBoolean(msg);
			}
		}.execute(null, null, null);
	}
	
	private void setInGame() {
		new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				if (KeyValueAPI.isServerAvailable()) {
					KeyValueAPI.put(GcmConstants.MHEALTH_TEAM_NAME, 
									GcmConstants.MHEALTH_PASS,
									registeredDevices.get(regid) + GAME_STATUS,
									"true");
					
					return "true";
				} else {
					return ERROR_TAG;
				}
			}

			@Override
			protected void onPostExecute(String msg) {
				if (msg.equals(ERROR_TAG)) {
					Toast.makeText(getApplicationContext(), NO_INTERNET_ERROR, Toast.LENGTH_SHORT).show();
				} else {
					Toast.makeText(getApplicationContext(), "Setting status to in-game", Toast.LENGTH_SHORT).show();
				}
			}
		}.execute(null, null, null);
	}
	
	private void setNotInGame() {
		new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				if (KeyValueAPI.isServerAvailable()) {
					KeyValueAPI.put(GcmConstants.MHEALTH_TEAM_NAME, 
									GcmConstants.MHEALTH_PASS,
									registeredDevices.get(regid) + GAME_STATUS,
									"false");
					
					return "false";
				} else {
					return ERROR_TAG;
				}
			}

			@Override
			protected void onPostExecute(String msg) {
				if (msg.equals(ERROR_TAG)) {
					Toast.makeText(getApplicationContext(), NO_INTERNET_ERROR, Toast.LENGTH_SHORT).show();
				} else {
					Toast.makeText(getApplicationContext(), "Setting status to out-of-game", Toast.LENGTH_SHORT).show();
				}
			}
		}.execute(null, null, null);
	}
	
	public static void storeAsyncGame(final Context c) {
		new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				if (KeyValueAPI.isServerAvailable()) {
					KeyValueAPI.put(GcmConstants.MHEALTH_TEAM_NAME, 
									GcmConstants.MHEALTH_PASS,
									registeredDevices.get(regid) + GAME_BOARD,
									AsyncGame.boardTiles);
					KeyValueAPI.put(GcmConstants.MHEALTH_TEAM_NAME, 
									GcmConstants.MHEALTH_PASS,
									registeredDevices.get(opponentRegid) + GAME_BOARD,
									AsyncGame.boardTiles);
					KeyValueAPI.put(GcmConstants.MHEALTH_TEAM_NAME, 
									GcmConstants.MHEALTH_PASS,
									registeredDevices.get(regid) + GAME_PLAYER,
									AsyncGame.playerTiles);
					KeyValueAPI.put(GcmConstants.MHEALTH_TEAM_NAME, 
									GcmConstants.MHEALTH_PASS,
									registeredDevices.get(regid) + GAME_BUNCH,
									AsyncGame.bunchTiles);
					
					String msg = "Opponent's points: " + AsyncGame.pointsScored;
					KeyValueAPI.put(GcmConstants.MHEALTH_TEAM_NAME,
							GcmConstants.MHEALTH_PASS,
							PROPERTY_ALERT_TEXT, msg);
					KeyValueAPI.put(GcmConstants.MHEALTH_TEAM_NAME,
							GcmConstants.MHEALTH_PASS,
							PROPERTY_TITLE_TEXT, "Update!");
					KeyValueAPI.put(GcmConstants.MHEALTH_TEAM_NAME,
							GcmConstants.MHEALTH_PASS,
							PROPERTY_CONTENT_TEXT, 
							msg);
					
					return msg;
				} else {
					return ERROR_TAG;
				}
			}

			@Override
			protected void onPostExecute(String msg) {
				if (msg.equals(ERROR_TAG)) {
					Toast.makeText(c, "Error: No internet connection", Toast.LENGTH_SHORT).show();
				} else {
					CommunicationNotification sender = new CommunicationNotification();
					Map<String, String> data = new HashMap<String, String>();
					ArrayList<String> recipients = new ArrayList<String>();
					recipients.add(opponentRegid);
					sender.sendNotification(data, recipients, c);
					Toast.makeText(c, "Moves sent!", Toast.LENGTH_SHORT).show();
				}
			}
		}.execute(null, null, null);
	}
	
	public static void loadAsyncGame() {
		new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				if (KeyValueAPI.isServerAvailable()) {
					AsyncGame.boardTiles = KeyValueAPI.get(GcmConstants.MHEALTH_TEAM_NAME, 
															GcmConstants.MHEALTH_PASS,
															registeredDevices.get(regid) + GAME_BOARD);
					KeyValueAPI.put(GcmConstants.MHEALTH_TEAM_NAME, 
									GcmConstants.MHEALTH_PASS,
									registeredDevices.get(opponentRegid) + GAME_BOARD,
									AsyncGame.boardTiles);
					KeyValueAPI.put(GcmConstants.MHEALTH_TEAM_NAME, 
									GcmConstants.MHEALTH_PASS,
									registeredDevices.get(regid) + GAME_PLAYER,
									AsyncGame.playerTiles);
					KeyValueAPI.put(GcmConstants.MHEALTH_TEAM_NAME, 
									GcmConstants.MHEALTH_PASS,
									registeredDevices.get(regid) + GAME_BUNCH,
									AsyncGame.bunchTiles);
					
					String msg = "Board tiles are: " + AsyncGame.boardTiles;
					KeyValueAPI.put(GcmConstants.MHEALTH_TEAM_NAME,
							GcmConstants.MHEALTH_PASS,
							PROPERTY_ALERT_TEXT, msg);
					KeyValueAPI.put(GcmConstants.MHEALTH_TEAM_NAME,
							GcmConstants.MHEALTH_PASS,
							PROPERTY_TITLE_TEXT, "Update!");
					KeyValueAPI.put(GcmConstants.MHEALTH_TEAM_NAME,
							GcmConstants.MHEALTH_PASS,
							PROPERTY_CONTENT_TEXT, 
							msg);
					
					return msg;
				} else {
					return ERROR_TAG;
				}
			}

			@Override
			protected void onPostExecute(String msg) {
				if (msg.equals(ERROR_TAG)) {
//					Toast.makeText(c, "Error: No internet connection", Toast.LENGTH_SHORT).show();
				} else {
					CommunicationNotification sender = new CommunicationNotification();
					Map<String, String> data = new HashMap<String, String>();
					ArrayList<String> recipients = new ArrayList<String>();
					recipients.add(opponentRegid);
//					sender.sendNotification(data, recipients, c);
				}
			}
		}.execute(null, null, null);
	}
	
	public static void storeSimulGame(final Context c) {
		new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				if (KeyValueAPI.isServerAvailable()) {
					KeyValueAPI.put(GcmConstants.MHEALTH_TEAM_NAME, 
									GcmConstants.MHEALTH_PASS,
									registeredDevices.get(regid) + GAME_BOARD,
									SimulGame.boardTiles);
					KeyValueAPI.put(GcmConstants.MHEALTH_TEAM_NAME, 
									GcmConstants.MHEALTH_PASS,
									registeredDevices.get(opponentRegid) + GAME_BOARD,
									SimulGame.boardTiles);
					KeyValueAPI.put(GcmConstants.MHEALTH_TEAM_NAME, 
									GcmConstants.MHEALTH_PASS,
									registeredDevices.get(regid) + GAME_PLAYER,
									SimulGame.playerTiles);
					KeyValueAPI.put(GcmConstants.MHEALTH_TEAM_NAME, 
									GcmConstants.MHEALTH_PASS,
									registeredDevices.get(regid) + GAME_BUNCH,
									SimulGame.bunchTiles);
					
					String msg = "Opponent's points: " + SimulGame.pointsScored;
					KeyValueAPI.put(GcmConstants.MHEALTH_TEAM_NAME,
							GcmConstants.MHEALTH_PASS,
							PROPERTY_ALERT_TEXT, msg);
					KeyValueAPI.put(GcmConstants.MHEALTH_TEAM_NAME,
							GcmConstants.MHEALTH_PASS,
							PROPERTY_TITLE_TEXT, "Update!");
					KeyValueAPI.put(GcmConstants.MHEALTH_TEAM_NAME,
							GcmConstants.MHEALTH_PASS,
							PROPERTY_CONTENT_TEXT, 
							msg);
					
					return msg;
				} else {
					return ERROR_TAG;
				}
			}

			@Override
			protected void onPostExecute(String msg) {
				if (msg.equals(ERROR_TAG)) {
					Toast.makeText(c, "Error: No internet connection, could not save progress!", Toast.LENGTH_SHORT).show();
				} else {
					CommunicationNotification sender = new CommunicationNotification();
					Map<String, String> data = new HashMap<String, String>();
					ArrayList<String> recipients = new ArrayList<String>();
					recipients.add(opponentRegid);
					sender.sendNotification(data, recipients, c);
				}
			}
		}.execute(null, null, null);
	}
	
	public static void loadGame() {
		new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				if (KeyValueAPI.isServerAvailable()) {
					// TODO: Figure out which game mode to load to
					
					KeyValueAPI.get(GcmConstants.MHEALTH_TEAM_NAME, 
									GcmConstants.MHEALTH_PASS,
									registeredDevices.get(regid) + GAME_BOARD);
					KeyValueAPI.get(GcmConstants.MHEALTH_TEAM_NAME, 
									GcmConstants.MHEALTH_PASS,
									registeredDevices.get(regid) + GAME_PLAYER);
					KeyValueAPI.get(GcmConstants.MHEALTH_TEAM_NAME, 
									GcmConstants.MHEALTH_PASS,
									registeredDevices.get(regid) + GAME_BUNCH);
					
					String msg = "Board tiles are: " + SinglePlayerGame.boardTiles;
					KeyValueAPI.put(GcmConstants.MHEALTH_TEAM_NAME,
							GcmConstants.MHEALTH_PASS,
							PROPERTY_ALERT_TEXT, msg);
					KeyValueAPI.put(GcmConstants.MHEALTH_TEAM_NAME,
							GcmConstants.MHEALTH_PASS,
							PROPERTY_TITLE_TEXT, "Update!");
					KeyValueAPI.put(GcmConstants.MHEALTH_TEAM_NAME,
							GcmConstants.MHEALTH_PASS,
							PROPERTY_CONTENT_TEXT, 
							msg);
					
					return msg;
				} else {
					return ERROR_TAG;
				}
			}

			@Override
			protected void onPostExecute(String msg) {
				if (msg.equals(ERROR_TAG)) {
//					Toast.makeText(getApplicationContext(), "Error: No internet connection", Toast.LENGTH_SHORT).show();
				} else {
					CommunicationNotification sender = new CommunicationNotification();
					Map<String, String> data = new HashMap<String, String>();
					ArrayList<String> recipients = new ArrayList<String>();
					recipients.add(opponentRegid);
					sender.sendNotification(data, recipients, context);
				}
			}
		}.execute(null, null, null);
	}
	
	private void findPartner() {
		new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				String msg = "";
				if (KeyValueAPI.isServerAvailable()) {
					updateNumOfRegDevices();
					
					for (int i = 1; i <= numOfRegDevices; i++) {
						opponentRegid = KeyValueAPI.get(GcmConstants.MHEALTH_TEAM_NAME, 
													  GcmConstants.MHEALTH_PASS,
													  "regid" + String.valueOf(i));
						Log.d(String.valueOf(i), opponentRegid);
						
						KeyValueAPI.put(GcmConstants.MHEALTH_TEAM_NAME,
								GcmConstants.MHEALTH_PASS,
								PROPERTY_ALERT_TEXT, "ALERT");
						KeyValueAPI.put(GcmConstants.MHEALTH_TEAM_NAME,
								GcmConstants.MHEALTH_PASS,
								PROPERTY_TITLE_TEXT, "ALERT!");
						KeyValueAPI.put(GcmConstants.MHEALTH_TEAM_NAME,
								GcmConstants.MHEALTH_PASS,
								PROPERTY_CONTENT_TEXT, "Looking for partner");
						
						opponentIsInGame();
						
						if (!opponentRegid.equals(regid) && !opponentInGame) {
							msg = opponentRegid;
							return msg; // Short circuit
						}
					}
				} else {
					msg = ERROR_TAG;
				}
				
				return msg;
			}

			@Override
			protected void onPostExecute(String msg) {
				String toastMsg;
				if (msg.isEmpty()) {
					toastMsg = "Couldn't find a partner... try again later!";
					Toast.makeText(getApplicationContext(), toastMsg, Toast.LENGTH_SHORT).show();
				} else if (msg.equals(ERROR_TAG)) {
					toastMsg = "No network connection, make sure you're connected to the internet!";
					Toast.makeText(getApplicationContext(), toastMsg, Toast.LENGTH_SHORT).show();
				} else {	
					toastMsg = "Found a partner: " + registeredDevices.get(msg);
					Toast.makeText(getApplicationContext(), toastMsg, Toast.LENGTH_SHORT).show();
					CommunicationNotification sender = new CommunicationNotification();
					Map<String, String> data = new HashMap<String, String>();
					List<String> ids = new ArrayList<String>();
					ids.add(msg);
					sender.sendNotification(data, ids, context);
					opponentFound = true;
				}
			}
		}.execute(null, null, null);
	}

	public static void setTopScore(int currentScore) {
		maybeHighScore = currentScore;
		new AsyncTask<Void, Void, String>() {
			
			@Override
			protected String doInBackground(Void... params) {
				String msg = "";
				if (KeyValueAPI.isServerAvailable()) {
					String currentHighScore = KeyValueAPI.get(GcmConstants.MHEALTH_TEAM_NAME, 
							GcmConstants.MHEALTH_PASS,
							TOP_SCORE);
					Log.v(TAG, "high score is:" + currentHighScore);
					if (currentHighScore.contains(ERROR_TAG) || Integer.parseInt(currentHighScore) < maybeHighScore) {
						KeyValueAPI.put(GcmConstants.MHEALTH_TEAM_NAME, 
										GcmConstants.MHEALTH_PASS,
										TOP_SCORE,
										String.valueOf(maybeHighScore));
						msg = String.valueOf(maybeHighScore);
					} else {
						msg = currentHighScore;
					}
				} else {
					msg = ERROR_TAG;
				}

				return msg;
			}

			@Override
			protected void onPostExecute(String msg) {
				if (!msg.contains(ERROR_TAG)) {
					TopScores.scores = "The top score is: " + msg;
				}
			}
		}.execute(null, null, null);
	}
	
	private void getTopScores() {
		new AsyncTask<Void, Void, String>() {
			
			@Override
			protected String doInBackground(Void... params) {
				String msg = "";
				if (KeyValueAPI.isServerAvailable()) {
					updateNumOfRegDevices();
					
					for (int i = 1; i <= numOfRegDevices; i++) {
						String score = KeyValueAPI.get(GcmConstants.MHEALTH_TEAM_NAME, 
													  GcmConstants.MHEALTH_PASS,
													  "regid" + String.valueOf(i) + TOP_SCORE);
						Log.d(TAG, "regid" + i + " score: " + score);
						
						if (!score.contains(ERROR_TAG))
							msg.concat("regid" + i + ":" + score + ",");
					}
				} else {
					msg = ERROR_TAG;
				}
				
				return msg;
			}

			@Override
			protected void onPostExecute(String msg) {
				if (msg.contains(ERROR_TAG)) {
					Toast.makeText(getApplicationContext(), NO_INTERNET_ERROR, Toast.LENGTH_SHORT).show();
				} else {
					TopScores.scores = msg;
				}
			}
		}.execute(null, null, null);
	}
	
	public static void opponentPeel() {
		new AsyncTask<Void, Void, String>() {
			
			@Override
			protected String doInBackground(Void... params) {
				String msg = "";
				if (!KeyValueAPI.isServerAvailable()) {
					msg = ERROR_TAG;
				}

				return msg;
			}

			@Override
			protected void onPostExecute(String msg) {
				if (!msg.isEmpty()) {
					CommunicationNotification sender = new CommunicationNotification();
					Map<String, String> data = new HashMap<String, String>();
					data.put("PEEL", "true");
					List<String> ids = new ArrayList<String>();
					ids.add(opponentRegid);
					sender.sendNotification(data, ids, context);
				}
			}
		}.execute(null, null, null);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		Music.play(this, R.raw.bananagrams_game, true);

		SharedPreferences settings = getSharedPreferences(SinglePlayerGame.BANANA_PREFS, 0);

		View continueSPButton = findViewById(R.id.wordgame_continue_sp_button);
		boolean continueSPGame = settings.getBoolean(SinglePlayerGame.CONTINUE_GAME, false);
		if (continueSPGame) {
			continueSPButton.setVisibility(View.VISIBLE);
			continueSPButton.setOnClickListener(this);
		} else {
			continueSPButton.setVisibility(View.GONE);
		}
		
//		SharedPreferences mpSettingsAsync = getSharedPreferences(AsyncGame.BANANA_PREFS, 0);
//		SharedPreferences mpSettingsSimul = getSharedPreferences(SimulGame.BANANA_PREFS, 0);
		
		View continueMPButton = findViewById(R.id.wordgame_continue_mp_button);
//		boolean continueMPGame = mpSettingsAsync.getBoolean(AsyncGame.CONTINUE_GAME, false) ||
//									mpSettingsSimul.getBoolean(SimulGame.CONTINUE_GAME, false);
//		if (continueMPGame) {
//			continueMPButton.setVisibility(View.VISIBLE);
//			continueMPButton.setOnClickListener(this);
//		} else {
			continueMPButton.setVisibility(View.GONE);
//		}
		
		setNotInGame();
	}

	@Override
	protected void onPause() {
		super.onPause();
		Music.stop(this);
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.bananagrams_new_button:
			openNewSPGameDialog();
			break;
		case R.id.wordgame_continue_sp_button:
			continueSPGame();
			break;
		case R.id.communication_find_player_button:
			Log.v(TAG, "finding partner");
			findPartner();
			break;
		case R.id.communication_new_mp_button:
			if (opponentFound) {
				openNewMPGameDialog();
			} else {
				Toast.makeText(getApplicationContext(), "No opponent found; try finding a player!", Toast.LENGTH_SHORT).show();
			}
			break;
		case R.id.wordgame_continue_mp_button:
			continueMPGame();
			break;
		case R.id.communication_top_score_button:
			Intent topScoresIntent = new Intent(this, TopScores.class);
			startActivity(topScoresIntent);
			break;
		case R.id.communication_help_button:
			Intent helpIntent = new Intent(this, Help.class);
			startActivity(helpIntent);
			break;
		case R.id.communication_acknowledgement_button:
			Intent acknowledgementsIntent = new Intent(this, Acknowledgements.class);
			startActivity(acknowledgementsIntent);
			break;
		case R.id.bananagrams_exit_button:
			finish();
			break;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.bananagrams_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.bananagrams_settings:
			startActivity(new Intent(this, Prefs.class));
			return true;
		}
		return false;
	}

	/** Ask the user what difficulty level they want */
	private void openNewSPGameDialog() {
		new AlertDialog.Builder(this)
		.setTitle(R.string.bananagrams_new_game_title)
		.setItems(R.array.bananagrams_difficulty,
				new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialoginterface,
					int i) {
				setInGame();
				startSPGame(i);
			}
		})
		.show();
	}
	
	private void openNewMPGameDialog() {
		new AlertDialog.Builder(this)
		.setTitle(R.string.communication_new_game_title)
		.setItems(R.array.communication_modes,
				new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialoginterface,
					int i) {
				setInGame();
				startMPGame(i);
			}
		})
		.show();
	}

	/** Start a new game with the given difficulty level **/
	private void startSPGame(int i) {
		Log.d(TAG, "Starting new game with difficulty " + i);
		Intent intent = new Intent(this, SinglePlayerGame.class);
		intent.putExtra(SinglePlayerGame.KEY_DIFFICULTY, i);
		startActivity(intent);
	}
	
	private void startMPGame(int i) {
		Log.d(TAG, "Starting new multiplayer game with mode " + i);
		if (i == SIMUL_GAME_MODE) {
			Intent intent = new Intent(this, SimulGame.class);
			startActivity(intent);
		} else if (i == ASYNC_GAME_MODE) {
			Intent intent = new Intent(this, AsyncGame.class);
			startActivity(intent);
		}
	}

	/** Continue a saved game **/
	private void continueSPGame() {
		Log.d(TAG, "Continue saved (sp) game");
		Intent intent = new Intent(this, SinglePlayerGame.class);
		intent.putExtra(SinglePlayerGame.CONTINUE_GAME, true);
		startActivity(intent);
	}
	
	private void continueMPGame() {
		Log.d(TAG, "Continue saved (mp) game");
		SharedPreferences mpSettingsAsync = getSharedPreferences(AsyncGame.BANANA_PREFS, 0);
		SharedPreferences mpSettingsSimul = getSharedPreferences(SimulGame.BANANA_PREFS, 0);
		
		 if (mpSettingsAsync.getBoolean(AsyncGame.CONTINUE_GAME, false)) {
			 Intent intent = new Intent(this, AsyncGame.class);
			 intent.putExtra(AsyncGame.CONTINUE_GAME, true);
			 startActivity(intent);
		 } else if (mpSettingsSimul.getBoolean(SimulGame.CONTINUE_GAME, false)) {
			 Intent intent = new Intent(this, SimulGame.class);
			 intent.putExtra(SimulGame.CONTINUE_GAME, true);
			 startActivity(intent);
		 }
		
	}
}