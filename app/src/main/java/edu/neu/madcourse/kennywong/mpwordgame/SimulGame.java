/***
 * Excerpted from "Hello, Android",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material, 
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose. 
 * Visit http://www.pragmaticprogrammer.com/titles/eband3 for more book information.
***/
package edu.neu.madcourse.kennywong.mpwordgame;

import java.util.ArrayList;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import edu.neu.madcourse.kennywong.R;
import edu.neu.madcourse.kennywong.bananagrams.GridAdapter;
import edu.neu.madcourse.kennywong.bananagrams.Music;
import edu.neu.madcourse.kennywong.bananagrams.data.BananagramsLetters;
import edu.neu.madcourse.kennywong.bananagrams.data.BananagramsTile;
import edu.neu.madcourse.kennywong.utils.Utils;

public class SimulGame extends Activity implements OnClickListener {
   private static final String TAG = "SimultaneousGame";

   public static final String KEY_DIFFICULTY = "bananagrams_difficulty";
   public static final String KEY_MULTIPLAYER_MODE = "communication_mode";

   public static final String BANANA_PREFS = "BananagramsPrefs_Simul";
   public static final String CONTINUE_GAME = "continue_game";
   public static final String LAST_BOARD = "last_board";
   public static final String TIME_LEFT = "time_remaining";
   public static final String POINTS = "points";
   public static final String BUNCH = "bunch";
   public static final String PLAYER_LETTERS = "player_letters";
   
   public static String playerTiles;
   public static String bunchTiles;
   public static String boardTiles;
   
   public static int pointsScored = 0;
   public static int opponentsPoints = 0;
   public static int tilesLeft = 0;
   public static int opponentsTilesLeft = 0;
   
   private int boardPosition = -1;
   private ArrayList<View> playerTileButtons = new ArrayList<View>();
   
   private boolean undo = false;
   private boolean dump = false;
   
   private static GridAdapter gridAdapter;
   
   private SensorManager mSensorManager;
   private ShakeEventListener mSensorListener;
   
   private static final int TILES_REVEALED_PER_SHAKE = 3;
   private static final int MAX_TILES = 21;
   private int numShakes = 0;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.simultaneous_wordgame);
      Log.d(TAG, "onCreate");

      gridAdapter = new GridAdapter(this);
      
      BananagramsLetters.initialize();
      GridAdapter.getBlankTiles();
      
      // Continue-specific values
      if (getIntent().getBooleanExtra(CONTINUE_GAME, false)) {
    	  Log.v(TAG, "Continuing game");
    	  SharedPreferences settings = getSharedPreferences(BANANA_PREFS, 0);
    	  
    	  String tilesAsString = BananagramsTile.toString(GridAdapter.tiles);
    	  GridAdapter.tiles = BananagramsTile.fromString(settings.getString(LAST_BOARD, tilesAsString));
    	  BananagramsLetters.loadBunch(settings.getString(BUNCH, BananagramsLetters.storeBunch()));
    	  BananagramsLetters.loadPlayerLetters(settings.getString(PLAYER_LETTERS, BananagramsLetters.storePlayerLetters()));
      }

//      TextView score = (TextView)findViewById(R.id.multiplayer_wordgame_your_score);
//      pointsScored = 0;
//      score.setText(String.valueOf(pointsScored));
      
      GridView grid = (GridView)findViewById(R.id.bananagrams_board);
      grid.setAdapter(gridAdapter);
      grid.setOnItemClickListener(new OnItemClickListener() {
          public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
        	  if (undo) {
        		  BananagramsLetters.returnLetter(GridAdapter.tiles.get(position).getChar());
        		  GridAdapter.tiles.set(position, new BananagramsTile());
        		  
        		  updatePlayerTileButtons();
        	  } else {
	        	  if (boardPosition == -1) {
	        		  GridAdapter.tiles.get(position).setSelected(true);
	        		  boardPosition = position;
	        	  } else if (boardPosition == position) {
	        		  GridAdapter.tiles.get(boardPosition).setSelected(false);
	        		  boardPosition = -1;
	        	  } else {
	        		  GridAdapter.tiles.get(boardPosition).setSelected(false);
	        		  GridAdapter.tiles.get(position).setSelected(true);
	        		  boardPosition = position;
	        	  }
        	  }
        	  
    		  updateGrid();
          }
      });
      
      SharedPreferences settings = getSharedPreferences(SimulGame.BANANA_PREFS, 0);
      settings.edit().putBoolean(CONTINUE_GAME, true).apply();
      
      View saveButton = findViewById(R.id.mp_wordgame_save_button);
      saveButton.setOnClickListener(this);
      View forfeitButton = findViewById(R.id.mp_wordgame_forfeit_button);
      forfeitButton.setOnClickListener(this);
      
      updatePlayerTileButtons();
      
      mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
      mSensorListener = new ShakeEventListener();   

      mSensorListener.setOnShakeListener(new ShakeEventListener.OnShakeListener() {
    	  public void onShake() {
    		  if (numShakes <= (MAX_TILES / TILES_REVEALED_PER_SHAKE)) {
    			  numShakes++;
    			  updatePlayerTileButtons();
    		  }
//    		  Toast.makeText(getApplicationContext(), "Shake #" + numShakes, Toast.LENGTH_SHORT).show();
    	  }
      });
   }

   @Override
   protected void onResume() {
	   super.onResume();

	   mSensorManager.registerListener(mSensorListener,
			   mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
			   SensorManager.SENSOR_DELAY_UI);
   }

   @Override
   protected void onPause() {
	   Log.d(TAG, "onPause");
	   SharedPreferences settings = getSharedPreferences(SimulGame.BANANA_PREFS, 0);
	   Editor editor = settings.edit();
	   editor.putString(LAST_BOARD, BananagramsTile.toString(GridAdapter.tiles)).apply();
	   editor.putString(BUNCH, BananagramsLetters.storeBunch()).apply();
	   editor.putString(PLAYER_LETTERS, BananagramsLetters.storePlayerLetters()).apply();
	   mSensorManager.unregisterListener(mSensorListener);
	   super.onPause();
   }
   
   protected void updateGrid() {
	   if (gridAdapter != null) {
		   Log.v(TAG, "Updating grid");
		   gridAdapter.notifyDataSetChanged();
		   
		   playerTiles = BananagramsLetters.storePlayerLetters();
		   bunchTiles = BananagramsLetters.storeBunch();
		   boardTiles = BananagramsTile.toString(GridAdapter.tiles);
		   
		   TextView score = (TextView)findViewById(R.id.multiplayer_wordgame_your_score);
		   score.setText("Your score: " + String.valueOf(pointsScored));
		   TextView oppScore = (TextView)findViewById(R.id.multiplayer_wordgame_their_score);
		   oppScore.setText("Their score: " + String.valueOf(opponentsPoints));
		   TextView oppTilesLeft = (TextView)findViewById(R.id.multiplayer_wordgame_their_tiles_left);
		   oppTilesLeft.setText("Opponent's tiles left: " + String.valueOf(opponentsTilesLeft));
		   tilesLeft = BananagramsLetters.playerLettersList().size();

		   if (WordGameMenu.instantiated)
			   WordGameMenu.storeSimulGame(this.getApplicationContext());
	   } else {
		   Log.v(TAG, "Could not update grid");
	   }
   }
   
   private void updatePlayerTileButtons() {
	   if (BananagramsLetters.playerLettersList().isEmpty()) {
		   BananagramsLetters.peel();
		   // WordGameMenu.opponentPeel();
	   }
	   
	   createPlayerTileViews();
	   setPlayerTileListeners();
   }
   
   private void createPlayerTileViews() {
	   playerTileButtons.clear();
	   LinearLayout ll = (LinearLayout)findViewById(R.id.bananagrams_keypad);
	   ll.removeAllViews();
	   
	   int i = 0;
	   for (Character c : BananagramsLetters.sortedPlayerLettersList()) {
		   Button b = new Button(this);
		   b.setId(Utils.generateViewId());
		   if (i < numShakes * TILES_REVEALED_PER_SHAKE || i >= MAX_TILES) {
			   b.setText(c.toString().toUpperCase(Locale.US));
		   } else {
			   b.setText(" ");
		   }

		   LinearLayout.LayoutParams params = 
				   new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 
						   LinearLayout.LayoutParams.WRAP_CONTENT);

		   b.setLayoutParams(params);
		   ll.addView(b);
		   playerTileButtons.add(b);
		   i++;
	   }
   }
   
   private void setPlayerTileListeners() {
	   for (int i = 0; i < BananagramsLetters.sortedPlayerLettersList().size(); i++) {
		   final char t = BananagramsLetters.sortedPlayerLettersList().get(i);
		   playerTileButtons.get(i).setOnClickListener(new View.OnClickListener(){
			   public void onClick(View v) {
				   if (dump) {
					   BananagramsLetters.dump(t);
					   updatePlayerTileButtons();
				   } else {
					   if (((Button)v).getText().toString().equals(" ")) {
						   ((Button)v).setText(String.valueOf(t));
					   }
					   if (boardPosition != -1) {
						   returnPlayerTileResult(t); // may not do anything if nothing selected on board
						   updatePlayerTileButtons();
					   } 
				   }
			   }});
	   }
   }
   
   private void updatePlayerTileViews(Character c) {
	   BananagramsLetters.useLetter(c);
	   GridAdapter.tiles.get(boardPosition).setSelected(false);
	   boardPosition = -1;

	   updatePlayerTileButtons();
   }

   /** Return the chosen tile to the caller */
   private void returnPlayerTileResult(char tile) {
	   if (boardPosition != -1) {
		   if (GridAdapter.tiles.get(boardPosition).getChar() != ' ') {
			   BananagramsLetters.returnLetter(GridAdapter.tiles.get(boardPosition).getChar());
		   }
		   GridAdapter.tiles.get(boardPosition).setChar(tile);
		   updateGrid();
		   updatePlayerTileViews(tile);
		   Music.play(this, R.raw.bananagrams_click, false);
	   }
   }

   private void gameOver() {
	   GridView grid = (GridView)findViewById(R.id.bananagrams_board);
	   grid.setEnabled(false);
	   String message = "Game over! You scored " + pointsScored + " points!";
	   Toast.makeText(this, message, Toast.LENGTH_LONG).show();
	   WordGameMenu.setTopScore(pointsScored);
   }
   
   @Override
   public void onClick(View v) {
	   switch (v.getId()) {
	   case R.id.bananagrams_save_game_button:
		   onPause();
		   finish();
		   break;
	   case R.id.mp_wordgame_forfeit_button:
		   // Game over!
		   SharedPreferences sets = getSharedPreferences(BANANA_PREFS, 0);
		   sets.edit().putBoolean(CONTINUE_GAME, false).apply();
		   gameOver();
		   finish();
		   break;
	   }
   }

   public void onToggleReturn(View view) {
	   Log.v(TAG, "toggle undo");
	   undo = ((ToggleButton) view).isChecked();
   }

   public void onToggleDump(View view) {
	   Log.v(TAG, "toggle dump");
	   dump = ((ToggleButton) view).isChecked();
   }

}
