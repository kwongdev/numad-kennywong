package edu.neu.madcourse.kennywong.bananagrams;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import edu.neu.madcourse.kennywong.R;

public class PauseMenu extends Activity implements OnClickListener {
	protected void onCreate(Bundle savedInstanceState)  {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bananagrams_pause);

		View settingsButton = findViewById(R.id.bananagrams_settings_button);
		settingsButton.setOnClickListener(this);

		View resumeButton = findViewById(R.id.bananagrams_resume_button);
		resumeButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.bananagrams_settings_button:
			Intent settingsIntent = new Intent(this, Prefs.class);
			startActivity(settingsIntent);
			break;
		case R.id.bananagrams_resume_button:
			finish();
			break;
		}
	}
}
