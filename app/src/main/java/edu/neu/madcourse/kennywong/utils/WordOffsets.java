package edu.neu.madcourse.kennywong.utils;

public class WordOffsets {
	private int bytesToDataStart;
	private int lengthOfGroup;
	
	public WordOffsets(int bytesToDataStart, int lengthOfGroup) {
		this.setBytesToDataStart(bytesToDataStart);
		this.setLengthOfGroup(lengthOfGroup);
	}

	public int getBytesToDataStart() {
		return bytesToDataStart;
	}

	public void setBytesToDataStart(int bytesToDataStart) {
		this.bytesToDataStart = bytesToDataStart;
	}

	public int getLengthOfGroup() {
		return lengthOfGroup;
	}

	public void setLengthOfGroup(int lengthOfGroup) {
		this.lengthOfGroup = lengthOfGroup;
	}
	
}
