package edu.neu.madcourse.kennywong.runmania;

import android.app.ActionBar;
import android.content.Intent;
import android.media.MediaMetadataRetriever;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.Locale;

import edu.neu.madcourse.kennywong.R;

public class Results extends FragmentActivity implements OnClickListener{
	public static final String TAG = "Results";
	
	static ArrayList<String> songPaths;
	static ArrayList<Integer> low_heartrate;
	static ArrayList<Integer> on_heartrate;
	static ArrayList<Integer> high_heartrate;
	
	static int target_heartrate;
	
	ResultsCollectionPagerAdapter mResultsCollectionPagerAdapter;

    ViewPager mViewPager;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.runmania_results);

		Intent intent = getIntent();
		songPaths = intent.getStringArrayListExtra(RunmaniaConstants.SONG_PATHS);
		low_heartrate = intent.getIntegerArrayListExtra(RunmaniaConstants.LOW_HR);
		on_heartrate = intent.getIntegerArrayListExtra(RunmaniaConstants.ON_HR);
		high_heartrate = intent.getIntegerArrayListExtra(RunmaniaConstants.HIGH_HR);
		target_heartrate = intent.getIntExtra(RunmaniaConstants.HEART_RATE, 0);

		Button quitButton = (Button)findViewById(R.id.runmania_quit_button);
		quitButton.setOnClickListener(this);
		
		mResultsCollectionPagerAdapter = new ResultsCollectionPagerAdapter(getSupportFragmentManager());

        // Set up action bar.
        final ActionBar actionBar = getActionBar();

        // Specify that the Home button should show an "Up" caret, indicating that touching the
        // button will take the user one step up in the application's hierarchy.
        actionBar.setDisplayHomeAsUpEnabled(false);

        // Set up the ViewPager, attaching the adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mResultsCollectionPagerAdapter);
	}
	
	@Override
	public void onBackPressed() {
		Intent intent = new Intent(getApplicationContext(), RunmaniaMenu.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		finish();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.runmania_quit_button:
			Intent intent = new Intent(getApplicationContext(), RunmaniaMenu.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			finish();
			break;
		}
	}
	
	/**
     * A {@link android.support.v4.app.FragmentStatePagerAdapter} that returns a fragment
     * representing an object in the collection.
     */
    public static class ResultsCollectionPagerAdapter extends FragmentStatePagerAdapter {

        public ResultsCollectionPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            Fragment fragment = new ResultsSectionFragment();
            Bundle args = new Bundle();
            args.putInt(ResultsSectionFragment.LOW_HR, low_heartrate.get(i));
            args.putInt(ResultsSectionFragment.ON_HR, on_heartrate.get(i));
            args.putInt(ResultsSectionFragment.HIGH_HR, high_heartrate.get(i));
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public int getCount() {
            return songPaths.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
        	MediaMetadataRetriever mmr = new MediaMetadataRetriever();
            String songPath = songPaths.get(position).split("/", 2)[1];
    		mmr.setDataSource(songPath);

    		String title = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
    		if (title == null || title.length() == 0) {
                title = new File(songPath).getName();
    		}
        	return title;
        }
    }
	
	public static class ResultsSectionFragment extends Fragment {
        public static final String LOW_HR = "low_hr";
        public static final String ON_HR = "on_hr";
        public static final String HIGH_HR = "high_hr";

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.runmania_results_fragment, container, false);
            Bundle args = getArguments();
            
            int low_song_heartrate = args.getInt(LOW_HR);
            int on_song_heartrate = args.getInt(ON_HR);
            int high_song_heartrate = args.getInt(HIGH_HR);
            int total_heartrate_calc = 
    				low_song_heartrate + 
    				on_song_heartrate + 
    				high_song_heartrate;

    		String withinTarget = "0.0";
    		String belowTarget = "0.0";
    		String aboveTarget = "0.0";
    		if (total_heartrate_calc != 0) {
    			withinTarget = String.format(Locale.US, "%.1f", ((double)on_song_heartrate/total_heartrate_calc) * 100.0);
    			belowTarget = String.format(Locale.US, "%.1f", ((double)low_song_heartrate/total_heartrate_calc) * 100.0);
    			aboveTarget = String.format(Locale.US, "%.1f", ((double)high_song_heartrate/total_heartrate_calc) * 100.0);
    		}
    		TextView target = (TextView)rootView.findViewById(R.id.runmania_target_hr);
    		target.setText(target_heartrate + " BPM");
    		TextView withinRange = (TextView)rootView.findViewById(R.id.runmania_within_range);
    		withinRange.setText(withinTarget + "%");
    		TextView belowRange = (TextView)rootView.findViewById(R.id.runmania_below_range);
    		belowRange.setText(belowTarget + "%");
    		TextView aboveRange = (TextView)rootView.findViewById(R.id.runmania_above_range);
    		aboveRange.setText(aboveTarget + "%");
    		
            return rootView;
        }
    }
}
