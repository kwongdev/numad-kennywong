package edu.neu.madcourse.kennywong.dictionary;

import java.util.Locale;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;
import edu.neu.madcourse.kennywong.R;
import edu.neu.madcourse.kennywong.utils.Wordlist;

public class DictionaryTextWatcher implements TextWatcher {
	private TextView output;
	private Context context;
	
	public DictionaryTextWatcher(TextView textView, Context context) {
		this.output = textView;
		this.context = context;
	}
	
	@Override
	public void afterTextChanged(Editable s) {
		// NO-OP
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		// NO-OP
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		String candidateWord = s.toString().toLowerCase(Locale.US).trim();
		
		if ((candidateWord.length() >= 3) && Wordlist.isValid(candidateWord)) {
			Music.play(context, R.raw.dictionary_correct);
			output.append(candidateWord + "\n");
		}
	}

}
