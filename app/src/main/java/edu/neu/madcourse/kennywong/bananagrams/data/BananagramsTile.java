package edu.neu.madcourse.kennywong.bananagrams.data;

import java.util.ArrayList;

public class BananagramsTile {
	private char c;
	private boolean horiPaint;
	private boolean vertPaint;
	private boolean startOfHoriWord;
	private boolean startOfVertWord;
	private boolean selected;
	
	public BananagramsTile(char c, boolean horiPaint, boolean vertPaint, boolean startOfHoriWord, boolean startOfVertWord, boolean selected) {
		this.setChar(c);
		this.setHoriPaint(horiPaint);
		this.setVertPaint(vertPaint);
		this.setStartOfHoriWord(startOfHoriWord);
		this.setStartOfVertWord(startOfVertWord);
		this.setSelected(selected);
	}
	
	public BananagramsTile() {
		this.setChar(' ');
		this.setHoriPaint(false);
		this.setVertPaint(false);
		this.setStartOfHoriWord(false);
		this.setStartOfVertWord(false);
		this.setSelected(false);
	}

	public BananagramsTile(String bt) {
		this.setChar(bt.charAt(0));
		this.setHoriPaint(bt.charAt(1) == 1);
		this.setVertPaint(bt.charAt(2) == 1);
		this.setStartOfHoriWord(bt.charAt(3) == 1);
		this.setStartOfVertWord(bt.charAt(4) == 1);
		this.setSelected(bt.charAt(5) == 1);
	}

	public static int getScore(ArrayList<BananagramsTile> arr) {
		int score = 0;

		for (BananagramsTile tile : arr) {
			if (tile.isStartOfHoriWord()) {
				score++;
			}
			if (tile.isStartOfVertWord()) {
				score++;
			}
		}

		return score;
	}

	public static char[] getChars(ArrayList<BananagramsTile> arr) {
		char[] res = new char[arr.size()];

		for (int i = 0; i < arr.size(); i++) {
			res[i] = arr.get(i).getChar();
		}

		return res;
	}

	public static String toString(ArrayList<BananagramsTile> arr) {
		String res = "";
		for (int i = 0; i < arr.size(); i++) {
			res += arr.get(i).toString();
			res += ",";
		}

		return res;
	}

	public static ArrayList<BananagramsTile> fromString(String s) {
		String[] stringList = s.split(",");

		ArrayList<BananagramsTile> tileList = new ArrayList<BananagramsTile>();
		for (int i = 0; i < stringList.length; i++) {
			tileList.add(new BananagramsTile(stringList[i]));
		}

		return tileList;
	}
	
	@Override
	public String toString() {
		return String.valueOf(getChar()) + 
				String.valueOf(boolToInt(isHoriPaint())) +
				String.valueOf(boolToInt(isVertPaint())) +
				String.valueOf(boolToInt(isStartOfHoriWord())) + 
				String.valueOf(boolToInt(isStartOfVertWord())) + 
				String.valueOf(boolToInt(isSelected())); 
	}
	
	public char getChar() {
		return c;
	}

	public void setChar(char c) {
		this.c = c;
	}

	public boolean isHoriPaint() {
		return horiPaint;
	}

	public void setHoriPaint(boolean horiPaint) {
		this.horiPaint = horiPaint;
	}

	public boolean isVertPaint() {
		return vertPaint;
	}

	public void setVertPaint(boolean vertPaint) {
		this.vertPaint = vertPaint;
	}

	public boolean isStartOfHoriWord() {
		return startOfHoriWord;
	}

	public void setStartOfHoriWord(boolean startOfHoriWord) {
		this.startOfHoriWord = startOfHoriWord;
	}

	public boolean isStartOfVertWord() {
		return startOfVertWord;
	}

	public void setStartOfVertWord(boolean startOfVertWord) {
		this.startOfVertWord = startOfVertWord;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	private static int boolToInt(boolean b) {
		if (b)
			return 1;
		return 0;
	}
	
	
}
