package edu.neu.madcourse.kennywong.bananagrams;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;
import edu.neu.madcourse.kennywong.bananagrams.data.BananagramsCheck;
import edu.neu.madcourse.kennywong.bananagrams.data.BananagramsTile;
import edu.neu.madcourse.kennywong.mpwordgame.AsyncGame;
import edu.neu.madcourse.kennywong.mpwordgame.SimulGame;

public class GridAdapter extends BaseAdapter {
	public static final int COLUMNS = 7;
    public static final int SIZE = COLUMNS * 10;
	
    private Context context;
    
    public static ArrayList<BananagramsTile> tiles = new ArrayList<BananagramsTile>();

    public GridAdapter(Context context) {
        this.context = context;
    }
    
    public static void getBlankTiles() {
    	tiles.clear();
    	for (int i = 0; i < SIZE; i++) {
    		tiles.add(new BananagramsTile());
    	}
    }
    
    public static int getNumRows() {
		return (int)Math.ceil(GridAdapter.tiles.size() / GridAdapter.COLUMNS);
	}

    public int getCount() {
        return tiles.size();
    }

    public Object getItem(int position) {
        return tiles.get(position);
    }

    public long getItemId(int position) {
        return 0; // TODO: implement
    }

    public View getView(int position, View convertView, ViewGroup parent) {
    	TextView tv;
    	if (convertView == null) {
    		tv = new TextView(context);
    		tv.setLayoutParams(new GridView.LayoutParams(95, 95));
    	}
    	else {
    		tv = (TextView) convertView;
    	}
		
    	tv.setGravity(Gravity.CENTER);
    	tv.setTextSize(24);
		tv.setTextColor(Color.BLACK);
    	tv.setText(String.valueOf(tiles.get(position).getChar()));
    	
    	BananagramsCheck.validate(position);
    	// TODO: streamline below code... callback method? necessary to update scores
    	SinglePlayerGame.pointsScored = BananagramsTile.getScore(tiles);
    	AsyncGame.pointsScored = BananagramsTile.getScore(tiles);
    	SimulGame.pointsScored = BananagramsTile.getScore(tiles);
    	
    	
    	if (tiles.get(position).getChar() == ' ' && !tiles.get(position).isSelected()) {
    		tv.setBackgroundColor(Color.WHITE);
    	} else if (tiles.get(position).isHoriPaint() || tiles.get(position).isVertPaint()) {
    		tv.setBackgroundColor(0xCC00ff00); // 20% Green
    	} else {
    		tv.setBackgroundColor(0xCCff0000); // 20% Red
    	}
    	
    	if (tiles.get(position).isSelected()) {
    		tv.setBackgroundColor(Color.LTGRAY);
    	}
    	
    	return tv;
    }
}
