package edu.neu.madcourse.kennywong.runmania;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import edu.neu.madcourse.kennywong.R;

public class RunmaniaDescription extends Activity implements OnClickListener {
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.runmania_description);

		View runmaniaButton = findViewById(R.id.runmania_start_button);
		runmaniaButton.setOnClickListener(this);
		View quitButton = findViewById(R.id.runmania_quit_button);
		quitButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.runmania_start_button:
			Intent runmaniaIntent = new Intent(this, RunmaniaMenu.class);
			startActivity(runmaniaIntent);
			break;
		case R.id.runmania_quit_button:
			finish();
			break;
		}
	}
}
